<?php
require __DIR__ . '/__connect_db.php';
$pname = 'lookbook';
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include __DIR__ . '/__page_head.php' ?>
    <link rel="stylesheet" type="text/css" href="css/lookbook2.css">
</head>
<body>
<?php include __DIR__ . '/__page_header.php' ?>

<div class="green_bg container-fluid">

    <div class="switch_wrap">
        <div class="switch_btn switch_top" data-img="1">
            <div class="switch_word switch_top_word">SHARP</div>
        </div>
        <div class="switch_btn switch_right" data-img="2">
            <div class="switch_word switch_right_word">SWEET</div>
        </div>
        <div class="switch_btn switch_bottom" data-img="3">
            <div class="switch_word switch_bottom_word">ELEGANT</div>
        </div>
        <div class="switch_btn switch_left" data-img="4">
            <div class="switch_word switch_left_word">DREAMY</div>
        </div>
    </div>

    <div class="styleleft col-xs-1">
        <img src="images/lookbook/style_sharp_left.png" alt="">
    </div>
    <div class="container col-xs-10">

        <div class="img-left">
            <div class="wrap wrapleft">
                <div class="close_pd col-xs-6 pos1">
                    <div class="mask-wrap">
                        <img class="close_pd_img" src="images/lookbook/sharp＿1.jpg" alt="">
                        <div class="mask">
                            <img src="images/allproducts/shop91.jpg" alt="">
                            <h3>搶不走的光采</h3>
                            <h3>TWD 400</h3>
                            <h4>SIZE 內直徑約 : 1.7 cm 商品材質/特性:精鍍白K/K金</h4>
                            <a onclick='add_item(91)' class='btn btn-sm buy_btn'>
                                <span>加入購物車</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="close_pd col-xs-6 pos2" data-img="2">
                    <div class="mask-wrap">
                        <img class="close_pd_img" src="images/lookbook/sharp＿4.jpg" alt="">
                        <div class="mask">
                            <img src="images/allproducts/shop92.jpg" alt="">
                            <h3>無敵助攻王</h3>
                            <h3>TWD 140</h3>
                            <h4>SIZE 內直徑約 : 1.7 cm 商品材質/特性:精鍍白K/K金</h4>
                            <a onclick='add_item(92)' class='btn btn-sm buy_btn'>
                                <span>加入購物車</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="img-right">
            <div class="wrap wrapright">
                <div class="close_pd col-xs-6 pos3" data-img="3">
                    <div class="mask-wrap">
                        <img class="close_pd_img" src="images/lookbook/sharp＿2.jpg" alt="">
                        <div class="mask">
                            <img src="images/allproducts/shop94.jpg" alt="">
                            <h3>超級助攻王</h3>
                            <h3>TWD 160</h3>
                            <h4>SIZE 內直徑約 : 1.7 cm 商品材質/特性:精鍍白K/K金</h4>
                            <a onclick='add_item(94)' class='btn btn-sm buy_btn'>
                                <span>加入購物車</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="close_pd col-xs-6 pos4" data-img="4">
                    <div class="mask-wrap">
                        <img class="close_pd_img" src="images/lookbook/sharp＿3.jpg" alt="">
                        <div class="mask">
                            <img src="images/allproducts/shop93.jpg" alt="">
                            <h3>波浪旋律戒指</h3>
                            <h3>TWD 170</h3>
                            <h4>商品皆為實際拍攝 商品材質：S925純銀 內圍直徑SIZE約：1.8 cm</h4>
                            <a onclick='add_item(93)' class='btn btn-sm buy_btn'>
                                <span>加入購物車</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>
    <div class="styleright col-xs-1">
        <img src="images/lookbook/style_sharp_right.png" alt="">
    </div>


</div>
<script>
    var dreamy = $('.switch_left');
    var sharp = $('.switch_top');
    var sweet = $('.switch_right');
    var elegant = $('.switch_bottom');
    var img1 = $('.img1 img');
    var img2 = $('.img2 img');
    var img3 = $('.img3 img');
    var img4 = $('.img4 img');
    var styleleft = $('.styleleft img');
    var styleright = $('.styleright img');
    //    $(".switch_btn").click(function(){
    //        console.log($(this).data("img"));
    //    });
    var offset = 0;
    var sharpdata = [
    {
        sid:"91", 
        h3:"搶不走的光采", 
        h4:"SIZE 內直徑約 : 1.7 cm 商品材質/特性:精鍍白K/K金",
        price:"400"
    },
    {
        sid:"92",
        h3:"無敵助攻王",
        h4:"SIZE 內直徑約 : 1.7 cm 商品材質/特性:精鍍白K/K金",
        price:"140"
    },
    {
        sid:"94",
        h3:"超級助攻王",
        h4:"SIZE 內直徑約 : 1.7 cm 商品材質/特性:精鍍白K/K金",
        price:"160"
    },
    {
        sid:"93",
        h3:"波浪旋律戒指",
        h4:"商品皆為實際拍攝 商品材質：S925純銀 內圍直徑SIZE約：1.8 cm",
        price:"170"
    }
    ];

    var dreamydata = [
    {
        sid:"95", 
        h3:"星繫於你", 
        h4:"SIZE 內直徑約 : 1.7 cm 商品材質/特性 精鍍白K 奧地利水鑽",
        price:"480"
    },
    {
        sid:"96",
        h3:"超時空迷航",
        h4:"SIZE 內直徑約 : 1.7 cm 商品材質/特性:全純銀",
        price:"650"
    },
    {
        sid:"98",
        h3:"佔有慾",
        h4:"SIZE 內直徑約 : 1.6 cm 商品材質/特性:鍍銀",
        price:"300"
    },
    {
        sid:"97",
        h3:"永遠的相對論",
        h4:"內直徑約 : 1.7 cm 商品材質/特性:鍍銀",
        price:"380"
    }
    ];

    var elegantdata = [
    {
        sid:"103", 
        h3:"生活點滴", 
        h4:"商品皆為實際拍攝 商品材質：合金",
        price:"190"
    },
    {
        sid:"104",
        h3:"生命是一場美好的旅程",
        h4:"SIZE 內直徑約 : 1.7 cm 商品材質/特性:精鍍白K/K金 奧地利水鑽",
        price:"450"
    },
    {
        sid:"106",
        h3:"Carpe Diem字母手環",
        h4:"商品皆為實際拍攝 商品材質：銅 手環SIZE內直徑約：6.5 cm<br>Made In Korea",
        price:"250"
    },
    {
        sid:"105",
        h3:"超級助攻王",
        h4:"SIZE 內直徑約 : 1.7 cm 商品材質/特性:精鍍白K/K金",
        price:"160"
    }
    ];

    var sweetdata = [
    {
        sid:"99", 
        h3:"永遠的相對論", 
        h4:"SIZE 內直徑約 : 1.7 cm 商品材質/特性:鍍銀",
        price:"380"
    },
    {
        sid:"100",
        h3:"無敵助攻王",
        h4:"SIZE 內直徑約 : 1.7 cm 商品材質/特性:精鍍白K/K金",
        price:"140"
    },
    {
        sid:"102",
        h3:"幸福的祕密咒語",
        h4:"SIZE 內直徑約 : 1.7 cm 商品材質/特性:全純銀",
        price:"400"
    },
    {
        sid:"101",
        h3:"一點靈尾戒",
        h4:"SIZE 內直徑約 : 1.4 cm 商品材質/特性:鍍金/鍍銀",
        price:"350"
    }
    ];

    var switchdreamy = function () {

        if ($('.pos1 img').attr('src') === "images/lookbook/dreamy＿1.jpg") {
            return false;
        }

        styleright.css({'transform': 'translateY(-80%)', 'opacity': '0'});
        styleleft.css({'transform': 'translateY(-80%)', 'opacity': '0'});
        setTimeout(function () {
            styleright.attr("src", "images/lookbook/style_dreamy_right.png");
            styleleft.attr("src", "images/lookbook/style_dreamy_left.png");
        }, 200);
        setTimeout(function () {
            styleright.css({'transform': 'translateY(0)', 'opacity': '1'});
            styleleft.css({'transform': 'translateY(0)', 'opacity': '1'});
        }, 300);

        offset = (offset + 1) % 4;
        $('.close_pd .close_pd_img').each(function (i) {
            $(this).attr("src", "images/lookbook/dreamy＿" + ((offset + i ) % 4 + 1) + ".jpg");
        });

        $('.close_pd').removeClass(function () {
            // return $(this).attr('class').replace('close_pd col-xs-6','');
            return (this.className.match(/\b(pos\d{1,4})\b/g) || []).join(' ');
        }).each(function (i) {
            var offsetnum = (offset + i) % 4 + 1;
            $(this).addClass("pos" + offsetnum);
            var $mask = $(this).find('.mask');
            //$mask.attr('data-sid', dreamydata[offsetnum-1].sid);
            $mask.html( "<img src='images/allproducts/shop" + dreamydata[offsetnum-1].sid + ".jpg'>" +
                "<h3>" + dreamydata[offsetnum-1].h3 + "</h3>" +
                "<h3>TWD " + dreamydata[offsetnum-1].price + "</h3>" +
                "<h4>" + dreamydata[offsetnum-1].h4 + "</h4>" +
                "<a onclick='add_item(" + dreamydata[offsetnum-1].sid + ")' class='btn btn-sm buy_btn'>" + "<span>加入購物車</span></a>"
                )
        });
    };


    var switchsharp = function () {

        if ($('.pos1 img').attr('src') === "images/lookbook/sharp＿1.jpg") {
            return false;
        }
// -----------------------------------左右風格切換--------------------------------
        styleright.css({'transform': 'translateY(-80%)', 'opacity': '0'});
        styleleft.css({'transform': 'translateY(-80%)', 'opacity': '0'});
        setTimeout(function () {
            styleright.attr("src", "images/lookbook/style_sharp_right.png");
            styleleft.attr("src", "images/lookbook/style_sharp_left.png");
        }, 200);
        setTimeout(function () {
            styleright.css({'transform': 'translateY(0)', 'opacity': '1'});
            styleleft.css({'transform': 'translateY(0)', 'opacity': '1'});
        }, 300);

// -----------------------------------大圖切換--------------------------------
        offset = (offset + 1) % 4;
        $('.close_pd .close_pd_img').each(function (i) {
            $(this).attr("src", "images/lookbook/sharp＿" + ((offset + i ) % 4 + 1) + ".jpg");
        });

// -----------------------------------div位置切換--------------------------------
        $('.close_pd').removeClass(function () {
            // return $(this).attr('class').replace('close_pd col-xs-6','');
            return (this.className.match(/\b(pos\d{1,4})\b/g) || []).join(' ');
        }).each(function (i) {
            var offsetnum = (offset + i) % 4 + 1;
            var that = $(this);
            $(this).addClass("pos" + offsetnum);
            var $mask = $(this).find('.mask');
            //$mask.attr('data-sid', dreamydata[offsetnum-1].sid);
            $mask.html(
                "<img src='images/allproducts/shop" + dreamydata[offsetnum-1].sid + ".jpg'>" +
                "<h3>" + dreamydata[offsetnum-1].h3 + "</h3>" +
                "<h3>TWD " + dreamydata[offsetnum-1].price + "</h3>" +
                "<h4>" + dreamydata[offsetnum-1].h4 + "</h4>" +
                "<a onclick='add_item(" + dreamydata[offsetnum-1].sid + ")' class='btn btn-sm buy_btn'>" + "<span>加入購物車</span></a>"
            )
        });
    };

    var switchelegant = function () {

        if ($('.pos1 img').attr('src') === "images/lookbook/elegant＿1.jpg") {
            return false;
        }

        styleright.css({'transform': 'translateY(-80%)', 'opacity': '0'});
        styleleft.css({'transform': 'translateY(-80%)', 'opacity': '0'});
        setTimeout(function () {
            styleright.attr("src", "images/lookbook/style_elegant_right.png");
            styleleft.attr("src", "images/lookbook/style_elegant_left.png");
        }, 200);
        setTimeout(function () {
            styleright.css({'transform': 'translateY(0)', 'opacity': '1'});
            styleleft.css({'transform': 'translateY(0)', 'opacity': '1'});
        }, 300);

        offset = (offset + 1) % 4;
        $('.close_pd .close_pd_img').each(function (i) {
            $(this).attr("src", "images/lookbook/elegant＿" + ((offset + i ) % 4 + 1) + ".jpg");
        });
        $('.close_pd').removeClass(function () {
            // return $(this).attr('class').replace('close_pd col-xs-6','');
            return (this.className.match(/\b(pos\d{1,4})\b/g) || []).join(' ');
        }).each(function (i) {
            var offsetnum = (offset + i) % 4 + 1;
            $(this).addClass("pos" + offsetnum);
            var $mask = $(this).find('.mask');
            //$mask.attr('data-sid', dreamydata[offsetnum-1].sid);
            $mask.html( "<img src='images/allproducts/shop" + dreamydata[offsetnum-1].sid + ".jpg'>" +
                "<h3>" + dreamydata[offsetnum-1].h3 + "</h3>" +
                "<h3>TWD " + dreamydata[offsetnum-1].price + "</h3>" +
                "<h4>" + dreamydata[offsetnum-1].h4 + "</h4>" +
                "<a onclick='add_item(" + dreamydata[offsetnum-1].sid + ")' class='btn btn-sm buy_btn'>" + "<span>加入購物車</span></a>"
            )
        });
    };
    var switchsweet = function () {

        if ($('.pos1 img').attr('src') === "images/lookbook/sweet＿1.jpg") {
            return false;
        }

        styleright.css({'transform': 'translateY(-80%)', 'opacity': '0'});
        styleleft.css({'transform': 'translateY(-80%)', 'opacity': '0'});
        setTimeout(function () {
            styleright.attr("src", "images/lookbook/style_sweet_right.png");
            styleleft.attr("src", "images/lookbook/style_sweet_left.png");
        }, 200);
        setTimeout(function () {
            styleright.css({'transform': 'translateY(0)', 'opacity': '1'});
            styleleft.css({'transform': 'translateY(0)', 'opacity': '1'});
        }, 300);

        offset = (offset + 1) % 4;
        $('.close_pd .close_pd_img').each(function (i) {
            $(this).attr("src", "images/lookbook/sweet＿" + ((offset + i ) % 4 + 1) + ".jpg");
        });
        $('.close_pd').removeClass(function () {
            // return $(this).attr('class').replace('close_pd col-xs-6','');
            return (this.className.match(/\b(pos\d{1,4})\b/g) || []).join(' ');
        }).each(function (i) {
            var offsetnum = (offset + i) % 4 + 1;
            var that = $(this);
            $(this).addClass("pos" + offsetnum);
            var $mask = $(this).find('.mask');
            //$mask.attr('data-sid', dreamydata[offsetnum-1].sid);
            $mask.html( "<img src='images/allproducts/shop" + dreamydata[offsetnum-1].sid + ".jpg'>" +
                "<h3>" + dreamydata[offsetnum-1].h3 + "</h3>" +
                "<h3>TWD " + dreamydata[offsetnum-1].price + "</h3>" +
                "<h4>" + dreamydata[offsetnum-1].h4 + "</h4>" +
                "<a onclick='add_item(" + dreamydata[offsetnum-1].sid + ")' class='btn btn-sm buy_btn'>" + "<span>加入購物車</span></a>"
            )
        });
    };


    dreamy.on("click", switchdreamy);
    sharp.on("click", switchsharp);
    sweet.on("click", switchsweet);
    elegant.on("click", switchelegant);

//    if ($('.pos1 img').attr('src') === "images/lookbook/sharp＿1.jpg") {
//        $('.pos1').mouseover(function(){
//            $('.sharp01').css({'z-index':'99','background':'rgba(0,0,0,0.7)'})
//        })
//        $('.pos1').mouseout(function(){
//            $('.sharp01').css({'z-index':'-1','background':'rgba(0,0,0,0.0)'})
//        })
//    }


    function add_item(sid){

        var addCart = $('.addCart');
        $.get("add_to_cart.php",{sid:sid,qty:1},function(data){
//            alert("已經加入購物車囉!");
            calItems(data);
        }, "json");

        addCart.css('transform','scaleX(1)');
        setTimeout(function(){
//            addCart.fadeOut('slow', function(){
            addCart.css('transform','scaleX(0)')
        },1000)
    }


</script>
<?php include __DIR__ . '/__page_footer.php' ?>
</body>
</html>