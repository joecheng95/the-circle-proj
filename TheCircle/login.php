<?php
require __DIR__ . '/__connect_db.php';
$pname = 'login';
if (isset($_POST['form_type']) and $_POST['form_type'] == 'register') {

    $hash = ($_POST['email2'] . uniqid());
    $sql = "INSERT INTO `members`(
         `email`,
         `password`,  
         `created_at`
          ) VALUES ( ?, ?, NOW() )";

    $stmt = $mysqli->prepare($sql);
    if ($mysqli->error) {
        echo $mysqli->error;
        exit;
    }
    $stmt->bind_param("ss",
        $_POST['email2'],
        $_POST['password2']
    );

    $stmt->execute();
    $result = $stmt->affected_rows;
    $new_id = $mysqli->insert_id;

    if ($result == 1) {
        $_SESSION['user'] = array(
            'id' => $new_id,
            'email' => $_POST['email2'],
            'couponpt' => 0,
        )
            ?>
<script>
    window.parent.jQuery.colorbox.close();
    window.parent.location.href = window.parent.location.href;
</script>
<?php

    }exit;
}


//    /*
//        if($stmt->error){
//            echo $stmt->error;
//            exit;
//        }
//    */
//    //echo $sql;
//    $result = $stmt->affected_rows;
//
////    echo $result;
////    exit;
//}


if (isset($_POST['form_type']) and $_POST['form_type'] == 'login') {
    $sql = sprintf("SELECT * FROM `members` WHERE `email`='%s' AND `password`='%s'",
        $mysqli->escape_string($_POST['email']),
        $mysqli->escape_string($_POST['password'])
    );

//    echo $sql;
//exit;
    $rs = $mysqli->query($sql);
    $row = $rs->fetch_assoc();
    if ($row) {
        $_SESSION['user'] = $row;
        $msg = '登入成功';

//        header("Location: ./");

//        if (isset($_SESSION['come_from'])) {
//            header("Location: " . $_SESSION['come_from']);
//            unset($_SESSION['come_from']);
//        } else {
//            header("Location: ./");
//        }
        ?>
        <script>
        window.parent.jQuery.colorbox.close();
        window.parent.location.href = window.parent.location.href;
        </script>
<?php
        exit;
    } else {
        $msg = '密碼錯誤';
    }
} else {
    $_SESSION['come_from'] = $_SERVER['HTTP_REFERER'];
}
?>
<!doctype html>
<html lang="en">
<title>會員登入</title>
<?php include __DIR__ . '/__page_head.php' ?>

<style>
    .container {
        margin-top: 5%;
        width: 900px;
        background: url("images/login_bg.png") 0 0 no-repeat;
        padding: 0;
        border-radius: 3%;
    }

    .formWrap {
        height: 550px;
        padding: 10%;
        margin: 5%;
        background: rgba(256, 256, 256, 1);
        border-radius: 3%;
    }

    .leftWrap {
        transform: translateX(15px);
    }

    .rightWrap {
        transform: translateX(-15px);
    }

    .formTitle {
        font-size: 18px;
        font-weight: 450;
        text-align: center;
        padding: 3% 0 3% 0;
        margin-bottom: 20px;
        border-top: solid 1px #c9e2df;
        border-bottom: solid 1px #c9e2df;
    }

    .checkbox {
        font-size: 12px;
        color: #1b6d85;
    }

    button.btn-info {
        text-shadow: none !important;
        box-shadow: none !important;
        font-family: arial;
        background-color: white;
        background-image: none;
        color: black;
        width: 200px;
        border: solid 1px #9E9E9E;
        transition: all 0.2s linear;
        border-radius: 0px;
    }

    button.btn-info:hover, button.btn-info:focus {
        background-color: #c9e2e0 !important;
        /*color:white;*/
        color: black;
        border: solid 1px #c9e2e0;
    }
</style>

<body>
<div class="container">
    <div class="col-md-6">
        <div class="formWrap leftWrap">
            <p class="formTitle title_left">會員登入</p>
            <form method="post">
                <input type="hidden" name="form_type" value="login">
                <div class="form-group">
                    <label for="exampleInputEmail1">帳號（常用信箱）</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="E-mail"
                           value="<?= empty($_POST['email']) ? '' : htmlentities($_POST['email']) ?>">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">密碼</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="密碼"
                           value="<?= empty($_POST['password']) ? '' : htmlentities($_POST['password']) ?>">
                </div>
             


                <button type="submit" class="btn btn-info">會員登入</button>
            </form>
        </div>
    </div><!---------------登入表單-------------->

    <div class="col-md-6">
        <div class="formWrap rightWrap">
            <p class="formTitle title_right">第一次購物</p>
            <form method="post">
                <input type="hidden" name="form_type" value="register">
                <div class="form-group">
                    <label for="exampleInputEmail1">帳號（常用信箱）</label>
                    <input type="email" class="form-control" id="email2" name="email2" placeholder="E-mail"
                           value="<?= empty($_POST['email2']) ? '' : htmlentities($_POST['email2']) ?>">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">密碼</label>
                    <input type="password" class="form-control" id="password2" name="password2" placeholder="密碼"
                           value="<?= empty($_POST['password2']) ? '' : htmlentities($_POST['password2']) ?>">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">確認密碼</label>
                    <input type="password" class="form-control" id="createInputPassword2" placeholder="再次輸入相同密碼">
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox"> 我已仔細閱讀並同意「會員服務條款」與「隱私權聲明」之內容
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox"> 我願意收到不定期優惠訊息
                    </label>
                </div>
                <button type="submit" class="btn btn-info">加入會員</button>
            </form>

        </div><!---------------註冊表單-------------->
    </div>
</div>
<script>
    var inline_content = $('#inline_content');

    $('#birthday').datepicker({
        dateFormat: "yy-mm-dd"
    });
    function checkForm(){

        var isPass = true;
        var email = $('#email');
        var password = $('#password');
        var nickname = $('#nickname');

        email.prev().hide();
        password.prev().hide();
        nickname.prev().hide();

        var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

        if(! pattern.test(email.val())){
            email.prev().show();
            isPass = false;
        }


        if(password.val().length < 3 ){
            password.prev().show();
            isPass = false;
        }

        if(nickname.val().length < 2 ){
            nickname.prev().show();
            isPass = false;
        }

        if(isPass){

            $.post('register2_back.php', $(document.form1).serialize(), function(data){
                //alert(data.info);
                var div = $('<div></div>');
                div.html(data.info);
                if(data.success){
                    div.css('color', 'green');
                } else{
                    div.css('color', 'red');

                }

                inline_content.html(div);

                $(".inline").click();
            }, 'json');



        }



        return false;
    }

    $(".inline").colorbox({inline:true, width:"50%"});


</script>
</body>
</html>