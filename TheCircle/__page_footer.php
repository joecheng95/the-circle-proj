


<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

<link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
<style>
    body {
        background-color: white;
    }

    .footer-distributed {
        box-shadow: 0 -2px 1px 0 #C9E2E0;
        box-sizing: border-box;
        width: 100%;
        text-align: left;
        font: bold 16px sans-serif;
        font-color: black;
        padding: 55px 50px;
        margin-top: 80px;
    }

    .footer-distributed .footer-left2,
    .footer-distributed .footer-left,
    .footer-distributed .footer-center,
    .footer-distributed .footer-right {
        display: inline-block;
        vertical-align: top;
    }

    /* Footer left */

    .footer-distributed .footer-left {
        text-align:left;
        width: 30%;
    }

    .footer-left {
        line-height: 25px;
    }

    .footer-distributed .footer-left2 {
        width: 25%;
    }

    /* The company logo */

    /*.footer-distributed h3 {*/
    /*color: #ffffff;*/
    /*font: normal 36px 'Cookie', cursive;*/
    /*margin: 0;*/
    /*}*/

    .footer-distributed h3 span {
        color: #5383d3;
    }

    /* Footer links */

    .footer-distributed .footer-links {
        color: #8f9296;
        margin: 20px 0 12px;
        padding: 0;
    }

    .footer-distributed .footer-links a {
        display: inline-block;
        line-height: 1.8;
        text-decoration: none;
        color: black;
    }

    .footer-distributed .footer-company-name {
        color: #8f9296;
        font-size: 14px;
        font-weight: normal;
        margin: 0;
    }

    /* Footer Center */

    .footer-distributed .footer-center {
        text-align:left;
        width: 25%;
    }

    .footer-distributed .footer-center i {
        background-color: #33383b;
        font-size: 25px;
        width: 38px;
        height: 38px;
        border-radius: 50%;
        text-align: center;
        line-height: 42px;
        margin: 10px 15px;
        vertical-align: middle;
    }

    .footer-distributed .footer-center i.fa-envelope {
        font-size: 17px;
        line-height: 38px;
    }

    .footer-distributed .footer-center p {
        display: inline-block;
        vertical-align: middle;
        margin: 0;
    }

    .footer-distributed .footer-center p span {
        display: block;
        font-weight: normal;
        font-size: 14px;
        line-height: 2;
    }

    .footer-distributed .footer-center p a {
        color: #5383d3;
        text-decoration: none;;
    }

    /* Footer Right */

    .footer-distributed .footer-right {
        width: 20%;
        text-align:left;
    }
    .footer-distributed .footer-right .fa{
        line-height:35px;
    }
    .footer-distributed .footer-company-about {
        line-height: 20px;
        color: black;
        font-size: 13px;
        font-weight: normal;
        margin: 0;
    }

    .footer-distributed .footer-company-about span {
        display: block;
        color: black;
        font-size: 16px;
        font-weight: bold;
        /*margin-bottom: 20px;*/
    }

    .footer-distributed .footer-icons {
        margin-top: 25px;
    }

    .footer-distributed .footer-icons a {
        display: inline-block;
        width: 35px;
        height: 35px;
        cursor: pointer;
        background-color: #33383b;
        border-radius: 50%;

        font-size: 20px;
        color: #ffffff;
        text-align: center;
        line-height: 35px ;

        margin-right: 3px;
        margin-bottom: 5px;
    }

    /*.delivery {*/
    /*padding-top: 40px;*/
    /*}*/

    .copyright {
        font-size: 12px;
        height: 100px;
        text-align: center;
        box-shadow: 0 -2px 1px 0 #C9E2E0;
    }

    .copyright p {
        padding-top: 20px;
    }

    /*.footer-center .payment p, .footer-center .delivery p {*/
    /*margin-bottom: 25px;*/
    /*}*/

    /* If you don't want the footer to be responsive, remove these media queries */

    @media (max-width: 880px) {

        .footer-distributed {
            font: bold 14px sans-serif;
        }

        .footer-distributed .footer-left,
        .footer-distributed .footer-center,
        .footer-distributed .footer-right {
            display: block;
            width: 100%;
            text-align: center;
        }

        .footer-distributed .footer-center {
            display:none;
        }
        .footer-distributed .footer-right p{
            display:none;
        }
        .footer-distributed{
            padding:20px 50px;
            margin-top:0;
        }
        body{
            font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
        }

    }
</style>
<body>

<!-- The content of your page would go here. -->

<footer class="footer-distributed">

    <div class="footer-left">
        <div>
            <p style="margin:0">COMPANY INFO</p>
            <p class="footer-links">
                <span>客服信箱 123@456.com</span>
                <br>
                <span>客服電話 02-3456-7890</span>
                <br>
                <span>服務時間 9:00AM~21:00AM</span>
                <br>
            </p>
        </div>
    </div>

    <div class="footer-center">

        <div class="payment">
            <p style="display:block">PAYMENT</p>
            <p style="display:block"><img src="images/payment.jpg"></p>
        </div>
        <br>
        <div class="delivery">
            <p style="display:block">DELIVERY</p>
            <p style="display:block"><img src="images/delivery.jpg"></p>
        </div>

    </div>

    <div class="footer-right">

        <p class="footer-company-about">
            <span>FOLLOW US</span>

        </p>

        <div class="footer-icons">

            <a href="#"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-linkedin"></i></a>
            <a href="#"><i class="fa fa-github"></i></a>

        </div>

    </div>

</footer>
<footer class=copyright>
    <p style="margin:0">COPYRIGHT &copy; 2017 THE CIRCLE ALL RIGHTS RESERVED.</p>
</footer>

</body>


