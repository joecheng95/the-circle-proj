<?php
//session_start();
//echo $_SESSION['coupon']==1 ? 'TTT' : 'FFF';
//exit;

require __DIR__ . '/__connect_db.php';
$pname = 'buy';

if(! isset($_SESSION['user']) or empty($_SESSION['cart'])) {
    header('Location: product_list.php');
    exit;
}

$amount = 0;


$keys = array_keys($_SESSION['cart']);

$sql = sprintf("SELECT * FROM `the circle` WHERE `sid` IN (%s)", implode(',', $keys));

$rs = $mysqli->query($sql);

while($row = $rs->fetch_assoc()){
    $row['qty'] = $_SESSION['cart'][$row['sid']]; // 取得某項商品的數量
    $c_prod[ $row['sid'] ] = $row;

    $amount += $row['qty']*$row['price'];
}
if(!empty($_SESSION['coupon']) and $_SESSION['coupon']==1){
    $sql = sprintf("UPDATE `members` SET `couponpt`=`couponpt`-50 WHERE `id`=%s",
        $_SESSION['user']['id']
    );
    $c_rs = $mysqli->query($sql);

    $sql = sprintf("INSERT INTO `orders`(`member_sid`, `amount`, `order_date`, `couponcheck`) VALUES (%s, %s, NOW(), 50)",
        $_SESSION['user']['id'],
        $amount-50
    );

    $mysqli->query($sql);
} else {
    $sql = sprintf("INSERT INTO `orders`(`member_sid`, `amount`, `order_date`) VALUES (%s, %s, NOW())",
        $_SESSION['user']['id'],
        $amount
    );

    $mysqli->query($sql);
}



$insert_id = $mysqli->insert_id; //拿到最新一筆資料的 主鍵


foreach($c_prod as $p){

    $sql = sprintf("INSERT INTO `order_details`(
        `order_sid`, `product_sid`, `price`, `quantity`
        ) VALUES (
          %s, %s, %s, %s
        )",
        $insert_id,
        $p['sid'],
        $p['price'],
        $p['qty']
    );

    $mysqli->query($sql);
}

unset($_SESSION['cart']);
unset($_SESSION['coupon']);
?>
<!doctype html>
<html lang="en">
<head>
    <?php include __DIR__ . '/__page_head.php' ?>
</head>


<style>
    .container {
        width: 900px;
        padding: 0;
        border-radius: 3%;
    }

    .formWrap {
        height: 550px;
        padding: 10%;
        margin: 5%;
        background: rgba(256, 256, 256, 1);
        border-radius: 3%;
    }

    .leftWrap {
        transform: translateX(15px);
    }

    .formTitle {
        font-size: 18px;
        font-weight: 450;
        text-align: center;
        padding: 3% 0 3% 0;
        margin-bottom: 20px;
        border-top: solid 1px #c9e2df;
        border-bottom: solid 1px #c9e2df;
    }

    .recipe_check {
        padding-bottom: 20px;
    }

    .recipe_check .recipeTitle {
        font-size: 16px;
        font-weight: 400;
        padding: 3% 0 1% 0;
        margin-bottom: 20px;
        border-bottom: solid 1px #c9e2df;
    }

    .circle {
        position: absolute;
        left: 27%;
        top: -110%;
        width: 500px;
        height: 500px;
        margin: 0 auto;
        border-radius: 50%;
        border: 0.2em solid #626262;
        background-color: white;
        transition:all 1s ease;

    }

    .circle .succ {
        display: block;
        margin: auto;
        margin-top: 130px;

    }

    .circle img {
        width: 50%;
    }

    .circle .text {
        text-align: center;
        padding-top: 45px;
    }

    .circle h1 {
        font-size: 30px;
        font-family: '微軟正黑體';
        color: #626262;
        letter-spacing: 2px;
    }

    .circle p {
        font-size: 20px;
        font-family: '微軟正黑體';
        color: #000000;
    }

    .circle .shop {
        color: rgb(204, 169, 112);
    }

    .circle .home {
        margin: auto;
        text-align: center;
        color: white;
        font-size: 15px;
    }

    /*.home-text{*/
    /*padding: 3.5px;*/
    /*font-family:'微軟正黑體';*/
    /*letter-spacing:3px;*/
    /*}*/

    .circle .circle_homebtn {
        width: 100px;
        display: inline-block;
        background-color: white;
        color:black;
        width:200px;
        border:solid 1px #9E9E9E;
        transition:all 0.2s linear;
        font-size: 15px;
        padding: 1%;
        font-family: '微軟正黑體';
    }

    .circle .circle_homebtn:hover{
        background-color:#c9e2e0;
        /*color:white;*/
        border:none;
    }
    a.btn-info {
        text-shadow: none !important;
        box-shadow: none !important;
        font-family: arial;
        background-color: white;
        background-image: none;
        color: black;
        width: 200px;
        border: solid 1px #9E9E9E;
        transition: all 0.2s linear;
        border-radius: 0px;
    }

    a.btn-info:hover, a.btn-info:focus {
        background-color: #c9e2e0 !important;
        /*color:white;*/
        color: black;
        border: solid 1px #c9e2e0;
    }


</style>

<body>
<div class="circle">
    <div>
        <img class="succ" src="images/Success.png" alt="">
        <div class="text">
            <h1>訂單已成立，感謝您的訂購!</h1>
            <p>如預查詢訂單，請至會員中心<a class="shop" href="history.php">訂購查詢</a></p>
        </div>
        <script>
            $('.shop').click(function(){
                window.parent.jQuery.colorbox.close();
                window.parent.location.href = 'history.php';
            })
        </script>
        <div class="home">

            <!--<div class="home-text">返回首頁</div>-->

            <div class="circle_homebtn">返回首頁</div>
        </div>
    </div>
</div>

<div class="container">
    <div class="col-md-12">
        <div class="formWrap leftWrap">
            <p class="formTitle">確認訂購資料</p>
            <form>
                <div class="form-group">
                    <label for="exampleInputEmail1">收件人姓名</label>
                    <input class="form-control" placeholder="姓名">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">手機</label>
                    <input class="form-control" placeholder="09XX-XXX-XXX">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">地址</label>
                    <input class="form-control" id="inputSecure" placeholder="請輸入地址">
                </div>
                <div class="recipe_check">
                    <p class="recipeTitle">發票資訊</p>
                    <label class="radio-inline"><input type="radio" name="inlineRadioOptions">電子發票
                    </label>
                    <label class="radio-inline"><input type="radio" name="inlineRadioOptions">捐贈發票
                    </label>
                    <label class="radio-inline"><input type="radio" name="inlineRadioOptions">統編發票
                    </label>
                </div>


                <a class="shopdone btn btn-info">確定訂購</a>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.shopdone').click(function () {
            $('.container').css('display', 'none');
            $('.circle').css('top', '10%')
        })
    });

    $('.circle_homebtn').click(function(){
        window.parent.jQuery.colorbox.close();
        window.parent.location.href = 'Home.php';
    })

</script>
</body>
</html>