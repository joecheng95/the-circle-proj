<!DOCTYPE html>
<html lang="en">
<head>
    <?php include __DIR__ . '/__page_head.php' ?>
    <link rel="stylesheet" type="text/css" href="css/customize.css">
</head>
<body>
<?php include __DIR__ . '/__page_header.php' ?>
<div>
    <div>
        <div>
            <div class="overhide">
                <div class="alphacont">
                    <div class="section-module alphabet">
                        <div class="text-container">
                            <div class="topic">
                                <h1>創造手鍊獨特造型</h1>
                            </div>
                            <div class="extopic">
                                以字母系列串飾拼湊您專屬的個人化風格珠寶
                            </div>
                        </div>
                        <div class="alphabet-bracelet mode-alphabet">
                            <img src="images/customize/l.png" alt="">
                            <img src="images/customize/o.png" alt="">
                            <img src="images/customize/v.png" alt="">
                            <img src="images/customize/e.png" alt="">
                        </div>
                        <div class="designer-options">
                            <div class="column center">
                                <div class="alphabet-input">
                                    <div class="input-container">
                                        <input id="alphabet-text" type="text" name="alphabet-text" pattern="[A-Z]" value maxlength="10">
                                    </div>
                                </div>
                                <div>英文拼字或數字(最多10個字母)</div>
                                <div class="relacont">
                                    <button class="btn btn-sm buy_btn" id="buy_btn" data-sid="107">加入購物車</button>
                                    <div class="cartsucc">加入成功</div>
                                </div>

                                <div class="total-price">$ 4,800</div>
                                <div class="col-md-12" id="error">
                                </div>
                            </div>
                        </div>
                        <div class="product-actions"></div>
                    </div>
                </div>
            </div>
            <div>

            </div>
        </div>
    </div>
    <div></div>
</div>
<script>

    var alphabet = $('.alphabet-bracelet.mode-alphabet');
    var alphatext = $('#alphabet-text').val("love");
    var alphaimg = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
//    function preventInput(e){
//        if(alphaimg.indexOf(e.which)===-1 ){
//            e.preventDefault();
//        }
//    }

//        --------------------------currentString為keyup前的input內容---------------------------
    var currentString = alphatext.val();

    alphatext.keydown(function (e) {

//        --------------------------keydown時防止按住---------------------------

//        if(e.originalEvent.repeat){
//        return false;
//        }

//        --------------------------keyup時直接防止不想要的字串輸入---------------------------

//        var i=0;
//        var my_str = alphatext.val();
//        var fil_str = '';
//        for(i=0; i<my_str.length; i++){
//            if(alphaimg.indexOf(my_str[i])>=0){
//                fil_str += my_str[i];
//            }
//        }
//        alphatext.val(fil_str);


//        --------------------------因為keydown後會在input字串顯示之前先執行抓取比對的function---------------------------
//        --------------------------因此加上setTimeout 0讓keydown事件先發生才執行function---------------------------

        setTimeout(function(){

//        --------------------------newString為keydown後的input內容---------------------------

            var newString = alphatext.val();
            var error = $('#error');
//        --------------------------imgNum為吊飾數量---------------------------

            var imgNum = $('.alphabet-bracelet').children().length;
//        --------------------------若字串等長則繼續執行---------------------------
        if(currentString.length === newString.length){
            return;
        }

//       --------------------------input增加數字或字母function---------------------------
            function addAlphabet() {
                for (let i = 0; i < newString.length; i++) {
                    if (currentString[i] !== newString[i]) {
                        var difString = newString[i].toString();


                        if(alphaimg.indexOf(difString)===-1){
                            error.append("<div class='alert alert-danger' role='alert'>抱歉, 我們並沒有此吊飾 </div>");
                            setTimeout(function(){
                                error.slideUp();
                            }, 3000);
                            alphatext.val(currentString);
                            return false;

//       --------------------------為避免i=0時eq(i-1)抓不到東西 將i=0拉出來單獨判斷---------------------------
                        }else if(i===0){
                            $("<img src='images/customize/" + difString + ".png'>").hide().insertBefore($('.alphabet-bracelet.mode-alphabet  img').eq(i)).fadeIn();
                            break;
                        }else{
                            $("<img src='images/customize/" + difString + ".png'>").hide().insertAfter($('.alphabet-bracelet.mode-alphabet  img').eq(i-1)).fadeIn();
                            break;
                        }

                    }
                }
            }

//       --------------------------一開始沒掛上吊飾時執行---------------------------
        if (imgNum === 0) {
            var firstString = newString.charAt(0).toString();

            if(alphaimg.indexOf(firstString)===-1){
//                $('#alphabet-text').keypress(function(e) {
//                    e.preventDefault();
//                });
                error.append("<div class='alert alert-danger' role='alert'>抱歉, 我們並沒有此吊飾 </div>");
                setTimeout(function(){
                    error.slideUp();
                }, 3000);
                return false;
            }else{$("<img src='images/customize/" + firstString + ".png'>").appendTo('.alphabet-bracelet.mode-alphabet').hide().fadeIn();
                currentString = newString;
                return;
            }
        }



//        --------------------------若input內數字或字母增加則執行addAlphabet 增加該吊飾---------------------------

        if (newString.length > currentString.length) {
            console.log("abc");
            addAlphabet();

//            --------------------------input內數字或字母減少則執行 移除該吊飾---------------------------

        } else {
            for (let i = 0; i < currentString.length; i++) {

                if (currentString[i] !== newString[i]) {
                    $('.alphabet-bracelet.mode-alphabet img').eq(i).fadeOut("fast", function(){
                        $(this).remove();
                    });
                    break;
                }
            }
        }

//        --------------------------將新的input字串更新至原有字串---------------------------

        currentString = newString;
        },0);


    });

    alphatext.keyup(function(){
        var dallorCommas = function(n){
            return '$ ' + n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        };
        var calTotalAmount = function(){
            var t = 0;
            var strNum = $('#alphabet-text').val().length;
            t += 1200*strNum;

            $('.total-price').text( dallorCommas(t) );
        };
        calTotalAmount();
    });

    $('#buy_btn').click(function(){
        var sid = $(this).attr('data-sid');
//        var qty = $(this).closest('.thumbnail').find('.qty').val();
        var qty = 1;
        var productname = "客製吊飾手鍊" + $('#alphabet-text').val();
        var addCart = $('.addCart');
        var cartsucc = $(this).closest('.relacont').find('.cartsucc');

        $.get('add_to_cart.php', {sid:sid, qty:qty}, function(data){
//            alert(productname + ' 已加入購物車');
            calItems(data); // 計算並顯示總數量
        }, 'json');

        cartsucc.css({'transform':'translateY(29px)', 'border':'solid 1px #a7a7a7', 'z-index':'2'});
        setTimeout(function(){
            cartsucc.css({'transform':'translateY(0)', 'border':'none', 'z-index':'-1'})
        },800)
//        addCart.css('transform','scaleX(1)');
//        setTimeout(function(){
////            addCart.fadeOut('slow', function(){
//            addCart.css('transform','scaleX(0)')
//        },1000)
    });
</script>
<?php include __DIR__ . '/Customization.php' ?>
<?php include __DIR__ . '/__page_footer.php' ?>
</body>
</html>
