<?php
require __DIR__ . '/__connect_db.php';
$pname = 'product_list';

$sid = isset($_GET['sid']) ? (int)$_GET['sid'] : 0;

$sql = "SELECT * FROM `the circle` WHERE `sid`=$sid";
$rs = $mysqli->query($sql);
$row = $rs->fetch_assoc();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include __DIR__ . '/__page_head.php' ?>
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- 	<link rel="stylesheet" href="css/bootstrap/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css"> -->
    <!-- 	<script src="js/jquery-3.2.1.js"></script>
        <script src="bootstrap/js/bootstrap.js"></script>
        <script src="https://code.jquery.com/ui/jquery-ui-git.js"></script>
        <script src="colorbox/jquery.colorbox.js"></script> -->
    <style>
        * {
            list-style: none;
            box-sizing: border-box;
            font-family: '微軟正黑體';

        }

        .wrapper {
            display: flex;
            justify-content: center;

        }

        .nav-left {
            padding-right: 50px;
            font-size: 18px;
            color: #959595;
            letter-spacing: 1px;
            display: inline-block;

        }

        .nav-left ul {
            padding-left: 0px;
        }

        .nav-left ul li {

            margin-bottom: 18px;
        }

        .nav-left ul li a {
            color: #959595;
        }

        .nav-left ul li a:active {
            color: rgb(204, 169, 112);
        }

        .title {
            color: #282828;
            margin: 5px 0;
        }

        .change h1 {
            font-size: 18px;
            border-bottom: 1px solid #000000;
            margin-bottom: 20px;
            padding-bottom: 5px;
        }

        .purchase-right {
            display: inline-block;
        }

        .nav-right {
            display: inline-block;
            padding-left: 50px;
            vertical-align: top;
        }

        .nav-right h2 {
            color: #959595;
            font-size: 18px;
        }

        p {
            color: 000000;
            font-size: 18px;
        }

        .price {
            color: rgb(204, 169, 112);
            display: block;
            margin-top: 40px;
            margin-bottom: 40px;
            font-size: 22px;
        }

        .change ul {
            padding-left: 0px;

        }

        .change ul li {
            display: inline-block;
            padding: 5px 10px;
            border: 1px solid #000000;
            text-align: center;
        }

        .ch1 {
            padding: 20px 0;
        }

        .num {
            padding: 9px 15px;
            border-radius: 10px;
            border: 1px solid #000000;
            text-align: center;
            margin: 10px 0;
        }
        .amount{
            text-align:center;
        }
        .amount input {
            text-align: center;
            width:70%;
        }

        .amount a {
            display: inline;
        }

        .add {
            padding: 10px 80px;
            background-color:rgb(204,169,112);
            border-radius: 10px;
            text-align: center;
            color: #ffffff;
            margin: 10px 0;
            word-spacing: 5px;
            border-width: 0;
        }

        .share {

            display: inline-block;
            padding: 4px 20px;
            border-radius: 10px;
            border: 1px solid rgb(204,169,112);

        }

        .wish {

            display: inline-block;
            padding: 5px 20px;
            margin: 10px 0;
            border-radius: 10px;
            border: 1px solid rgb(204,169,112);
        }

        .small-images img {
            margin: 20px 20px 20px 0;

        }

        .commodity h4, .gallery h4, .message h4, .recently-viewed h4 {
            font-size: 22px;
            padding-bottom: 10px;
            border-bottom: 1px solid #c9e2e0;
            margin-bottom: 20px;
        }

        .work {
            letter-spacing: 2px;
            word-spacing: 5px;

        }

        .recently img {
            padding: 10px;
        }

        .index {
            margin-bottom: 40px;
        }

        .maybe {
            display: inline-block;
            padding-left: 15%;
        }

        .maybe img {
            padding-right: 20px;
        }

        .pic3 {
            display: inline-block;
            padding-left: 20px;
        }

        .sent {
            color: #ffffff;
            display: inline-block;
            padding: 5px 20px;
            border-radius: 10px;
            background-color: rgb(204,169,112);
            border: 1px solid rgb(204,169,112);

        }

        .cancel {
            color: white;
            display: inline-block;
            padding: 5px 20px;
            margin: 10px 0;
            border-radius: 10px;
            background-color: rgb(204,169,112);
            border: 1px solid rgb(204,169,112);
        }

        .size {
            display: inline-block;
            padding: 5px 20px;
            margin: 10px 0;
            border-radius: 10px;
            border: 1px solid #000000;
        }

        .sss {
            display: inline-block;
            padding-right: 10px;
        }

        a {
            text-decoration: none;
        }

        .bigimg {

            display: inline-block;
        }

        .three > img {
            vertical-align: top;
        }

        .ch1 a {
            color: #000000;
        }

        .change a:active {
            color: rgb(204, 169, 112);
        }

        .size a {
            color: #000000;
        }
        button:focus{
            outline:none !important;
        }

        @media all and (max-width: 600px) {
            .wrapper{
                padding-top:5%;
            }
            .bigimg img{
                width:60% !important;
            }

            .nav-left {
                display: none;
            }

            .nav-right {
                text-align: center;
                width: 100%;
            }

            .num {
                margin:0 auto;
                width: 35%;
            }

            .pic3 {
                width: 100%;
            }

            .three > img {
                display: none;

            }

            .index {
                display: none;
            }
            .recently-viewed{
                display:none;
            }
            .maybelike{
                display:none;
            }
            .recently img {
                margin: 0px .5%;

                width: 48.5%;

            }

            .commodity {
                text-align: left;
            }
            .commodity h4{
                text-align:center;
            }

            .commodity h4, .gallery h4, .message h4, .recently-viewed h4 {
                padding-left: 2%;
            }

            .commodity p {
                font-size: 16px;
            }

            .nav-right {
                padding-left: 0px;
            }

            .message textarea {

                margin: 0px 2%;
            }

            .sent {
                margin: 0px 2%;
            }

            .small-images {
                margin: 0 2%;
            }

            .viewimages {
                text-align: center;
            }


        }


    </style>
</head>
<body>
<?php include __DIR__ . '/__page_header.php' ?>
<div class="wrapper">

    <!-- ----------左邊的nav bar--------- -->

    <div class="nav-left">

        <ul>
            <li class="title"><u>New Arrival</u></li>
            <li><a href="">本週新品</a></li>
            <li><a href="">熱銷補貨</a></li>
        </ul>

        <ul>
            <li class="title"><u>SALE</u></li>
            <li><a href="">套組優惠</a></li>
            <li><a href="">買一送一特區</a></li>
            <li><a href="">精選兩件59折</a></li>
        </ul>

        <ul>
            <li class="title"><u>全部商品</u></li>
            <li><a href="">戒指</a></li>
            <li><a href="">手環</a></li>
            <li><a href="">客製化商品</a></li>
        </ul>

    </div>

    <div class="purchase-right">
        <div class="viewimages">
            <div class="bigimg">
                <img style="width:100%;" src="images/product_img/demo/shop<?= $row['sid'] ?>_1.jpg" alt="">
            </div>
            <!-- ------------------------------------- -->
            <div class="nav-right">

                <h2>全部商品 > 戒指 > 刻字款</h2>
                <p><?= $row['name'] ?></p>
                <span class="price">TWD <?= $row['price'] ?></span>

                <div class="change">
                    <h1>請選擇數量</h1>

                    <div class="amount">
                            <a href=""> - </a>
                            <input type="text" value="1">
                            <a href=""> + </a>
                    </div>

                    <div class="addtobag">

                        <button class="add buy_btn" data-sid="<?= $row['sid'] ?>">ADD TO BAG</button>

                    </div>

                    <div class="share">

                        <img src="images/product_img/fb.png" alt=""> SHARE

                    </div>

                    <div class="wish">

                        <img src="images/product_img/heart.png" alt=""> WISH LIST

                    </div>

                    <div class="inform">

                        <div><i class="fa fa-volume-down" aria-hidden="true"></i> 現貨僅剩1件，即將售完！</div>

                    </div>


                </div>

            </div>
        </div>


        <div class="commodity">
            <h4>商品描述</h4>
            <p><?= $row['introduction'] ?></p>
            <p>建議可收存於密封夾鏈袋內，較可保持商品長久使用，請避免碰水</p>
            <p>顏色請參考單品圖片較為接近，但因圖檔顏色會因個人電腦螢幕設定差異略有不同，
                請以實際商品顏色為主．</p>
        </div>
        <br>

        <div class="gallery">
            <h4 class="work">GALLERY</h4>
            <div class="gallery-left">
                <div class="three">
                    <img style="width: 63%;" src="images/product_img/demo/shop<?= $row['sid'] ?>_2.jpg" alt="">
                    <div class="pic3">
                        <img style="width: 100%; padding-bottom:15px;" src="images/product_img/demo/shop<?= $row['sid'] ?>_3.jpg" alt="">
                        <div>
                            <img style="width: 100%;" src="images/product_img/demo/shop<?= $row['sid'] ?>_4.jpg" alt="">
                        </div>
                    </div>


                </div>

            </div>


        </div>

        <div class="message">
            <h4>留言板</h4>
            <textarea style="width: 80%; height: 200px;" id="" cols="30" rows="10">輸入您要留言的內容</textarea></br>
            <button class="sent">送出</button>
            <button class="cancel">取消</button>
        </div>

<!--        <div class="recently-viewed">-->
<!--            <h4 class="work">RECENTLY VIEWED</h4>-->
<!---->
<!--            <div class="recently">-->
<!--                <img class="index" src="images/product_img/多邊形 1.png" alt="">-->
<!--                <img src="images/product_img/thumb (12).png" alt="">-->
<!--                <img src="images/product_img/thumb (8).png" alt="">-->
<!--                <img src="images/product_img/0000.png" alt="">-->
<!--                <img src="images/product_img/thumb (12).png" alt="">-->
<!--                <img class="index" src="images/product_img/多邊形 1 拷貝.png" alt="">-->
<!--            </div>-->
<!---->
<!--        </div>-->

<!--        <div class="maybelike">-->
<!--            <h4 class="work">YOU MAY ALSO LIKE...</h4>-->
<!---->
<!--            <div class="maybe">-->
<!---->
<!--                <img src="images/product_img/11111.png" alt="">-->
<!--                <img src="images/product_img/thumb (8) 拷貝.png" alt="">-->
<!--                <img src="images/product_img/thumb (7).png" alt="">-->
<!---->
<!--            </div>-->
<!---->
<!--        </div>-->

    </div>

</div>
<script>
    $('.buy_btn').click(function () {
        var sid = $(this).attr('data-sid');
//        var qty = $(this).closest('.thumbnail').find('.qty').val();
        var qty = 1;
        var productname = $(this).closest('p').text();
        var addCart = $('.addCart');

        $.get('add_to_cart.php', {sid: sid, qty: qty}, function (data) {
//            alert(productname + ' 已加入購物車');
            calItems(data); // 計算並顯示總數量
        }, 'json');

        addCart.css('transform','scaleX(1)');
        setTimeout(function(){
//            addCart.fadeOut('slow', function(){
            addCart.css('transform','scaleX(0)')
        },1000)
    });
</script>
<?php include __DIR__ . '/__page_footer.php' ?>

</body>
</html>