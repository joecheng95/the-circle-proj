<?php
require __DIR__ . "/__connect_db.php";
$pname = "product_list";

$t_rs = $mysqli->query("SELECT COUNT(1) FROM `the circle`");
$t_rows = $t_rs->fetch_row();
$num_rows = $t_rows[0];

//取得該頁的資料
$page = isset($_GET["page"]) ? (int)$_GET["page"] : 1;
$per_page = 12;
$num_pages = ceil($num_rows / $per_page);

$sql = sprintf("SELECT * FROM `the circle` WHERE sid>50 LIMIT %s, %s", ($page - 1) * $per_page, $per_page);

$rs = $mysqli->query($sql);
?>

<!DOCTYPE html>
<html lang="en">
<?php include __DIR__ . '/__page_head.php' ?>

<style>

    .container-fluid .row .banner {
        text-align: center;
    }

    .container-fluid .row .product_wrap {
        padding-left: 15%;
        padding-right: 15%;
        margin-top: 3%;
        text-align: center;
    }

    .container-fluid .row .banner img {
        width: 100%;
    }

    .product_box {
        text-align: center;
        margin-bottom: 15%;
    }

    .product_box .product_pic img {
        width: 200px;
        height: auto;
        border: solid 1px #f3f3f3;
    }

    .product_box .product_name {
        margin: 5px 0 5px 0;
        font-size: 16px;
        font-weight: 400;
    }

    .product_box .price {
        margin: 5px 0 5px 0;
        color: #8a8a8a;
        font-size: 14px;
        font-weight: 400;
    }

    /*-----------------------timer--------------------*/
    .container-fluid .row .banner .banner_img {
        width: 70%;
        margin: 0 auto;
        position: relative;
    }

    .container-fluid .row .banner .sale_timer {
        color: #ee7f79;
        position: absolute;
        left: 40%;
        bottom: 4%;

    }

    .container-fluid .row .banner .sale_timer h3 {
        font-size: 3.5vw;
    }
    .buy_btn{
        position:relative;
        text-shadow:none !important;
        box-shadow:none !important;
        font-family:arial;
        background-color:white;
        background-image:none;
        color:black;
        width:200px;
        border:solid 1px #9E9E9E;
        transition:all 0.2s linear;
        border-radius:0px;
    }
    .buy_btn:hover{
        background-color:#c9e2e0;
        /*color:white;*/
        border:solid 1px #c9e2e0;
    }
    nav.stroke ul li:nth-child(1) a{
        color:rgb(204,169,112);
    }
    .cartsucc{
        font-size:12px;
        transition:all 0.3s linear;
        position:absolute;
        width:200px;
        /*border:solid 1px #9E9E9E;*/
        top:0;
        bottom:0;
        right:0;
        left:-1px;
        z-index:-1;
        color:black;
        background-color:white;
    }

    /*!*--------------media Q--------------------*!*/
    @media screen and (max-width: 600px) {
        .container-fluid .product_box .product_pic img {
            width: 100%;
        }

        .container-fluid .row {
            min-width: 100%;
        }

        .container-fluid .row .banner .banner_img {
            width: 100%;
        }

        .container-fluid .row .banner {
            padding-top: 5%;
        }

        .container-fluid .row .product_wrap {
            padding-top: 10%;
        }

        .container-fluid .row .banner .sale_timer {
            left: 35%;
            bottom: 4%;
        }

        .container-fluid .row .banner .sale_timer h3 {
            margin: 0;
            font-size: 5.5vw;
        }

        .container-fluid .col-xs-6 {
            height: 230px;
        }
        .buy_btn{
            width: 100px;
        }
        h3.product_name{
         font-size: 10px;
        }
        h3{
            text-align:left;
            width:150px;
        }
        .cartsucc{
            display:none;
        }
    }

</style>

<body>
<?php include __DIR__ . '/__page_header.php' ?>
<div class="container-fluid">

    <div class="row">

        <div class="col-md-12 col-xs-12 banner">
            <div class="banner_img">
                <img src="images/product_list/onsale_banner.jpg" alt="">
                <div class="sale_timer">
                    <h3>
                        <span id="hour"></span>
                        :
                        <span id="min"></span>
                        :
                        <span id="sec"></span>
                    </h3>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 product_wrap">

            <?php while ($row = $rs->fetch_assoc()): ?>

                <div class="col-md-3 col-xs-6">
                    <div class="product_box">
                        <div class="product_pic">
                            <a href="product.php?sid=<?= $row['sid'] ?>">
                                <img src="images/allproducts/shop<?= $row['sid'] ?>.jpg" alt="">
                            </a>
                        </div>
                        <h3 class="product_name"><?= $row['name'] ?></h3>
                        <h4 class="price">TWD<?= $row['price'] ?></h4>
                        <div class="relacont">
                            <a class="btn btn-sm buy_btn" data-sid="<?= $row['sid'] ?>">
                                <div class="cartsucc">加入成功</div><span>加入購物車</span>
                            </a>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    </div>

</div>


<?php include __DIR__ . '/__page_footer.php' ?>


<script type='text/javascript'>

    var startDate = new Date();
    var endDate = new Date('2017/08/03 8:00');
    var spantime = (endDate - startDate) / 1000;
    var digi2 = function(num){
        num = ''+num;

        return num.length===1 ? ('0'+num) : num;
    };

    $(document).ready(function () {
        EveryTime();
        setInterval(EveryTime, 1000);
        function EveryTime(i) {
            spantime--;
            var d = Math.floor(spantime / (24 * 3600));
            var h = Math.floor((spantime % (24 * 3600)) / 3600);
            var m = Math.floor((spantime % 3600) / (60));
            var s = Math.floor(spantime % 60);

            if (spantime > 0) {
                $('#hour').text(h + (d * 24));
                $('#min').text(digi2(m));
                $('#sec').text(digi2(s));
            } else { // 避免倒數變成負的
                $('#hour').text(0);
                $('#min').text(0);
                $('#sec').text(0);
            }
        }
    });
    $('.buy_btn').click(function () {
        var sid = $(this).attr('data-sid');
//        var qty = $(this).closest('.thumbnail').find('.qty').val();
        var qty = 1;
        var productname = $(this).parent().find('h3').text();
        var addCart = $('.addCart');
        var cartsucc = $(this).closest('.relacont').find('.cartsucc');

        $.get('add_to_cart.php', {sid: sid, qty: qty}, function (data) {
//            alert(productname + ' 已加入購物車');
            calItems(data); // 計算並顯示總數量
        }, 'json');

        cartsucc.css({'transform':'translateY(29px)', 'border':'solid 1px #a7a7a7'});
        setTimeout(function(){
            cartsucc.css({'transform':'translateY(0)', 'border':'none'})
        },800)

//        addCart.css('transform','scaleX(1)');
//        setTimeout(function(){
////            addCart.fadeOut('slow', function(){
//            addCart.css('transform','scaleX(0)')
//        },1000)

    });
</script>
</body>
</html>