<?php
require __DIR__ . '/__connect_db.php';
$pname = 'cart_list';

if (empty($_SESSION["cart"])) {
    $has_data = false;
} else {
    $keys = array_keys($_SESSION['cart']);

    $sql = sprintf("SELECT * FROM `the circle` WHERE `sid` IN (%s)", implode(',', $keys));

    $rs = $mysqli->query($sql);

    while ($row = $rs->fetch_assoc()) {
        $row['qty'] = $_SESSION['cart'][ $row['sid'] ]; // 取得某項商品的數量
        $c_prod[ $row['sid'] ] = $row;
    }
    $has_data = true;
//print_r($c_prod);
//exit;
}
?>
    <style>
        .container{
            max-width: 1000px;
        }
        td{
            font-size: 16px;
            font-weight: 350;
            color: dimgrey;
        }
        .del {
            color: rgb(204,169,112);
            cursor: pointer;
        }
        td img{
            max-width: 80px;
        }
        .del_box{
            max-width: 100px;
        }
        .tableTitle {
            background: black;
            color: white;
            font-size: 18px;
            font-weight: 450;
            text-align: center;
            padding: 1% 0 1% 0;
            margin-bottom: 20px;
            border-bottom: solid 1px #c9e2df;
        }
        .total_box{
            background: #c9e2e0;
            padding: 1%;
            margin-bottom: 2%;
            text-align: center;
            font-size: 16px;
            color: dimgrey;
        }
        button.btn-info {
            text-shadow: none !important;
            box-shadow: none !important;
            font-family: arial;
            background-color: white;
            background-image: none;
            color: black;
            width: 200px;
            border: solid 1px #9E9E9E;
            transition: all 0.2s linear;
            border-radius: 0px;
        }
        button.btn-info:hover, button.btn-info:focus{
            background-color:#c9e2e0 !important;
            /*color:white;*/
            color: black;
            border:solid 1px #c9e2e0;
        }
    </style>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include __DIR__ . '/__page_head.php' ?>
</head>
<?php include __DIR__ . '/__page_header.php' ?>
    <div class="container">



        <div class="col-md-12">
            <p class="tableTitle">訂單內容</p>
            <?php if ($has_data): ?>
            <table class="table table-hover">
                <thead>
                <tr>

                    <th>商品資訊</th>
                    <th>單價</th>
                    <th>數量</th>
                    <th>收藏</th>
                    <th>總計</th>
                </tr>
                </thead>

                <tbody>
                <?php foreach($c_prod as $item): ?>
                    <tr data-sid="<?= $item["sid"] ?>">

                        <td><img src="images/allproducts/shop<?= $item['sid'] ?>.jpg">
                            <?= $item['name'] ?>
<!--                            <br>--><?//= $item['introduction'] ?><!--</td>-->
                        <td class="price" data-val="<?= $item['price'] ?>"></td>
                        <td>
                            <select  class="qty">
                                <?php for($i=1; $i<=15; $i++): ?>
                                    <option value="<?=$i?>" <?= $i==$item['qty'] ? 'selected' : ''?>><?=$i?></option>
                                <?php endfor; ?>
                            </select>
                        </td>
                        <td></td>
                        <td class="sub-total"></td>
                    </tr>
                <?php endforeach; ?>

                </tbody>
            </table>
                <div class="total_box" role="alert">總計: <strong id="amount"></strong></div>

                <?php if(isset($_SESSION['user'])): ?>
                    <button class="buyinfo btn btn-info pull-right" href="history.php">查看其他訂單</button>
                <?php else: ?>
                    <a class="login btn btn-info pull-right" href="login.php">請先登入會員再結帳</a>
                <?php endif; ?>
                <script>
                    $('.buyinfo').colorbox({iframe:true, fixed:true,  innerWidth:"1000px", height:"750px", innerHeight:"800px"});
                    $('.login').colorbox({iframe:true, fixed:true,  innerWidth:"1000px", height:"750px", innerHeight:"800px"});
                </script>

            <?php else: ?>
                <div class="alert alert-danger" role="alert">目前購物車沒有任何商品</div>
            <?php endif; ?>
        </div>


    </div>
    <script>
        var dallorCommas = function(n){
            return '$ ' + n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        };

        $(".del").click(function () {
            var tr = $(this).closest("tr");
            var sid = tr.attr("data-sid");

            $.get('add_to_cart.php', {sid: sid}, function (data) {
                // location.href = location.href; // reload page,會閃一下跳頁
                calItems(data);
                tr.remove();
                calTotalAmount();
            }, 'json');
        });


        $('.qty').on('change', function(){
            var tr = $(this).closest('tr');
            var sid = tr.attr('data-sid');
            var qty = $(this).val();
            // console.log( $(this).val(), tr.attr('data-sid') );

            $.get('add_to_cart.php', {sid:sid, qty:qty}, function(data){
                var sub = tr.find('.sub-total');
                var price = tr.find('.price').attr('data-val');
                sub.text( dallorCommas(qty * price) );
                calItems(data);
                calTotalAmount();
            }, 'json');
        });

        $('.sub-total').each(function(){
            var tr = $(this).closest('tr');
            var price_td = tr.find('.price');
            var price = price_td.attr('data-val');
            var qty = tr.find('.qty').val();
            price_td.text( dallorCommas( price ) );
            $(this).text( dallorCommas( price*qty ) );
        });

        // 計算總價
        var calTotalAmount = function(){
            var t = 0;
            $('.sub-total').each(function(){
                var tr = $(this).closest('tr');
                var price_td = tr.find('.price');
                var price = price_td.attr('data-val');
                var qty = tr.find('.qty').val();
                t += price*qty;
            });

            $('#amount').text( dallorCommas(t) );
        };
        calTotalAmount();
    </script>
    <?php include __DIR__ . '/__page_footer.php' ?>