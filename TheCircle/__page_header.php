
<style>
    * {
        box-sizing: border-box;
    }
    .btn:focus, .btn:active{
        outline:none !important;
        outline-offset:0px;
    }


    body > header {
        text-align: center;
        padding-bottom:2%;
    }

    body {
        font-size: 18px;
        font-family: 'Cabin', '微軟正黑體';
        line-height: 150%;
    }
    h1.logo{
        margin:0;
    }

    nav.guide {
        text-align: right;
        max-width: 1600px;
        margin: auto;
        margin-top: 10px;
        display: block;
    }

    nav.guide ul {
        padding: 0;
        margin: 0;
        display: inline-block;
        zoom: 1;
        margin-right: 20px;
    }
    nav.guide ul li:nth-child(1) a{
        color:rgb(204,169,112);
    }
    nav.guide ul li a{
        color:black;
    }
    nav.guide ul li a:focus{
        text-decoration:none;
        outline:none;
        outline-offset:0px;
    }

    ul > li > a {
        text-decoration: none;
        border: none;
    }

    nav > ul > li {
        list-style-type: none;
        position: relative;
        /*float: left;*/
    }

    nav > ul > li > a:hover {
        opacity: 1;
        text-decoration: none;
    }

    nav.guide > ul > li:nth-child(1) > a:after, nav.guide > ul > li:nth-child(2) >a:after
    , nav.guide > ul > li:nth-child(3) > a:after,  nav.guide > ul > li:nth-child(4) > a:after {
        content: '／';
    }

    nav.guide > ul > li {
        float: left;
    }

    nav.stroke ul li a,
    nav.fill ul li a {
        position: relative;
    }

    nav.stroke ul li a:after, nav.fill ul li a:after {
        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
        width: 0%;
        content: '.';
        color: transparent;
        background: #aaa;
        height: 1px;
    }

    nav.stroke ul li a:hover:after {
        width: 100%;
    }

    nav.fill ul li a {
        transition: all 2s;
    }

    nav.fill ul li a:after {
        text-align: left;
        content: '.';
        margin: 0;
        opacity: 0;
    }

    nav.fill ul li a:hover {
        color: #fff;
        z-index: 1;
    }

    nav.fill ul li a:hover:after {
        z-index: -10;
        animation: fill 1s forwards;
        -webkit-animation: fill 1s forwards;
        -moz-animation: fill 1s forwards;
        opacity: 1;
    }

    nav.stroke {
        text-align: center;
        margin: 0 auto;
        background: #fff;
        padding: 20px 0 0 0;
        box-shadow: 0px 1px 0px #C9E2E0;
    }

    nav.stroke ul {
        display: inline-block;
        list-style: none;
        text-align: center;
        padding: 0;
    }

    nav.stroke ul li {
        display: inline-block;
    }

    nav.stroke ul li a {
        font-size: 18px;
        display: block;
        padding: 15px 20px 5px 20px;
        text-decoration: none;
        color: #000000;
        text-transform: uppercase;
        margin: 0 10px;
    }

    nav.stroke ul li a,
    nav.stroke ul li a:after,
    nav.stroke ul li a:before {
        transition: all 0.5s;
    }

    nav.stroke ul li a:hover {
        color: #555;
    }

    .fa-shopping-cart {
        font-size: 20px;
    }


    .c-hamburger {
        display: none;

    }

    .c-hamburger--htx {
        opacity:0.7;
        transform: scale(0.5);
        background-color: transparent;
    }

    .c-hamburger--htx span {
        transition: background 0s 0.3s;
    }

    .c-hamburger--htx span::before,
    .c-hamburger--htx span::after {
        transition-duration: 0.3s, 0.3s;
        transition-delay: 0.3s, 0s;
    }

    .c-hamburger--htx span::before {
        transition-property: top, transform;
    }

    .c-hamburger--htx span::after {
        transition-property: bottom, transform;
    }

    /* active state, i.e. menu open */
    .c-hamburger--htx.is-active {
        opacity:1;
        background: none;
    }

    .c-hamburger--htx.is-active span {
        background: none;
    }

    .c-hamburger--htx.is-active span::before {
        top: 0;
        transform: rotate(45deg);
    }

    .c-hamburger--htx.is-active span::after {
        bottom: 0;
        transform: rotate(-45deg);
    }

    .c-hamburger--htx.is-active span::before,
    .c-hamburger--htx.is-active span::after {
        transition-delay: 0s, 0.3s;
        background-color: white;
    }

    #NavTablet {
        min-height: 974px;
        display: none;
        z-index: 9999;
        background-color: black;
        opacity: 0.85;
        height: 100%;
        width: 100%;
        position: fixed;
    }

    #NavTablet ul {
        height:100%;
        margin: 0;
        padding: 35px 28px 0px 28px;
        list-style-type: none;
    }

    #NavTablet ul li {
        position:relative;
        font-size: 24px;
        letter-spacing: 1px;
        text-align: center;
        padding-top: 16%
    }

    #NavTablet ul li .botbar{
        width:0%;
        height:1px;
        position:absolute;
        bottom:-10px;
        left:20%;
        background: #020202; /* Old browsers */
        background: -moz-linear-gradient(left, #020202 0%, #ffffff 34%, #ffffff 63%, #000000 100%); /* FF3.6-15 */
        background: -webkit-linear-gradient(left, #020202 0%,#ffffff 34%,#ffffff 63%,#000000 100%); /* Chrome10-25,Safari5.1-6 */
        background: linear-gradient(to right, #020202 0%,#ffffff 34%,#ffffff 63%,#000000 100%);
    }

    #NavTablet ul li a {
        padding: 15px 20px 5px 20px;
        color:white;
        text-decoration: none;
    }


    #NavTablet ul li a:hover {
        color: white;
        z-index: 9999999;
    }

    #NavTablet ul li a:hover:after {
        z-index: -10;
        animation: fill 1s forwards;
        -webkit-animation: fill 1s forwards;
        -moz-animation: fill 1s forwards;
        opacity: 1;
    }

    #NavTablet ul li a:after,
    #NavTablet ul li a:before {
        transition: all 0.5s;
    }

    #NavTablet ul li a:after {
        z-index: 99999999999999;
        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
        width: 0%;
        content: '.';
        color: transparent;
        background: white;
        height: 1px;
    }

    .secnav {
        display: none;
    }

    /*----------------menu fixed-----------------*/
    nav.menu-fixed {
        display: none;
        box-shadow: 0px 2px 1px 0px #C9E2E0;
        background: white;
        position: fixed;
        z-index: 999;
        top: 0;
        left: 0;
        right: 0;
    }

    nav.menu-fixed > ul > li {
        float: left;
    }

    nav.menu-fixed ul {
        vertical-align: middle;
        padding: 0;
        margin: 0;
        display: inline-block;
        zoom: 1;
        margin-right: 20px;
    }

    nav.menu-fixed > ul > li > a {
        color: black;
        margin: 0px 10px;
        padding: 0px 20px 0px 20px;
    }

    .menu-fixed .menu-fixed-logo {
        display: inline-block;
    }

    .menu-fixed .menu-fixed-logo img {
        cursor: pointer;
        transform: scale(0.8);
        width: 60px;
    }
    .badge{
        margin-left:3px;
        margin-bottom:7px;
    }

    /*----------------media query-----------------*/
    @media all and (max-width: 600px) {

        .c-hamburger {
            z-index: 99999;
            top: 0;
            right: 0;
            display: block;
            position: fixed;
            overflow: hidden;
            margin: 0 auto;
            padding: 0;
            width: 96px;
            height: 96px;
            font-size: 0;
            text-indent: -9999px;
            appearance: none;
            box-shadow: none;
            border-radius: none;
            border: none;
            cursor: pointer;
            transition: background 0.3s;
        }

        .c-hamburger:focus {
            outline: none;
        }

        .c-hamburger span {
            display: block;
            position: absolute;
            top: 44px;
            left: 18px;
            right: 18px;
            height: 8px;
            background: black;
        }

        .c-hamburger span::before,
        .c-hamburger span::after {
            position: absolute;
            display: block;
            left: 0;
            width: 100%;
            height: 8px;
            background-color: black;
            content: "";
        }

        .c-hamburger span::before {
            top: -20px;
        }

        /*----------------nav-----------------*/
        .c-hamburger span::after {
            bottom: -20px;
        }

        nav.stroke {
            display: none;
        }

        nav.stroke ul li {
            background: aliceblue;
        }

        nav.guide {
            display: none;
            text-align: center;
        }

        h1.logo {
            margin: 0;
            /*padding-bottom: 25px;*/
            box-shadow: 0px 2px 0px #C9E2E0;
        }

        h1.logo img {
            transform: scale(0.6);
        }

        /*----------------secnav-----------------*/
        .secnav {
            display: block;
            position: fixed;
            bottom: 0;
            left: 0;
            width: 100%;
            z-index: 999;
        }

        .secnav-right {
            position: relative;
            right: 0;
            background: rgb(204, 169, 112);
            bottom: 0;
            height: 50px;
        }

        .secnav-right > ul {
            padding: 0;
            display: flex;
            list-style: none;
        }

        .secnav-right > ul > li {
            height: 50px;
            flex: 1;
            text-align: center;
            margin: 0;
            position: relative;
            display: block;
            float: left;
        }

        .secnav-right ul li a {
            color: white;
        }
        .secnav .secnav-right ul li a img{
            padding-top:7px;
            width:28px;
            height:33px;
        }

        .secnav-right .signIn > a:after, .secnav-right .members > a:after, .secnav-right .wish-list > a:after, .secnav-right .bag > a:after {
            display: block;
            width: 100%;
            position: absolute;
            left: 0;
            bottom: 3px;
            font-size: 16px;
            line-height: 12px;
            transform: scale(.703);
            white-space: nowrap;
        }

        .secnav-right .signIn > a:after {
            content: '會員登入';

        }

        .secnav-right .members > a:after {
            content: '會員中心';

        }

        .secnav-right .wish-list > a:after {
            content: '收藏清單';

        }

        .secnav-right .bag > a:after {
            content: '購物車';

        }

        .secnav-right i {
            transform: scale(1.5);
            padding-top: 9px;
        }

        .fa .fa-shopping-cart {
            padding-right: 4px;
        }

        nav.menu-fixed {
            display: none !important;
        }
    }

    @media all and (min-width: 601px) {
        #NavTablet {
            display: none !important;
        }
    }
</style>

<span id="NavTablet">
        <ul>
            <li>
                <a href="onsale.php">
                    最新優惠
                </a>
                <div class="botbar"></div>
            </li>
            <li>
                <a href="productlist.php">
                    全部商品
                </a>
            <div class="botbar"></div>
            </li>
            <li>
                <a href="lookbook.php">
                    形象特輯
                </a>
            <div class="botbar"></div>
            </li>
            <li>
                <a href="about.php">
                    關於我們
                </a>
            <div class="botbar"></div>
            </li>
            <li>
                <a href="customize.php">
                    客製服務
                </a>
            <div class="botbar"></div>
            </li>
        </ul>
    </span>
<header>
    <nav class="animated menu-fixed hideMenu">
        <div class="menu-fixed-logo">
            <img src="images/LOGO-2.png" alt="THE CIRCLE">
        </div>
        <ul>
            <li><a href="onsale.php">最新優惠</a></li>
            <li><a href="productlist.php">全部商品</a></li>
            <li><a href="lookbook.php">形象特輯</a></li>
            <li><a href="about.php">關於我們</a></li>
            <li><a href="customize.php">客製服務</a></li>
        </ul>
    </nav>
    <script>
        $(window).scroll(function () {
            var menuFixed = $('nav.menu-fixed');
            if ($(window).scrollTop() >= 258) {
                menuFixed.slideDown(100, function () {
                    menuFixed.css('display', 'block')
                })

            } else {
                menuFixed.slideUp(100, function () {
                    menuFixed.css('display', 'none');
                })
            }
        })

    </script>
    <nav class="guide">
        <ul class="profile">
            <?php if(isset($_SESSION['user'])): ?>
            <li><a href="logout.php">登出</a></li>
            <li><a href="history.php">會員中心</a></li>
            <li><a class="coupon" href="coupon.php">每日購物金</a></li>
            <li><a href="wishlist.php">收藏清單</a></li>
                <li><a href="cart_list.php" style="color:rgb(204,169,112)">購物車<i class="fa fa-shopping-cart" aria-hidden="true"></i><span class="badge t-badge" style="background-color:#8c8c8c"></span></a></li>
                <script>
                    $('.coupon').colorbox({onClosed: function(){window.parent.location.href = window.parent.location.href}, iframe:true, fixed:true,  innerWidth:"900px", height:"900px", innerHeight:"600px"});
                </script>
            <?php else: ?>
            <li><a class="login" href="login.php">登入</a></li>
            <li><a href="history.php">會員中心</a></li>
            <li><a class="coupon" href="coupon.php">每日購物金</a></li>
            <li><a href="wishlist.php">收藏清單</a></li>
                <li><a href="cart_list.php" style="color:rgb(204,169,112)">購物車<i class="fa fa-shopping-cart" aria-hidden="true"></i><span class="badge t-badge" style="background-color:#8c8c8c"></span></a></li>
                <script>
                    $('.login').colorbox({iframe:true, fixed:true,   innerWidth:"1000px", height:"750px", innerHeight:"800px"});
                    $('.coupon').colorbox({iframe:true, fixed:true,  innerWidth:"900px", height:"900px", innerHeight:"600px"});
                </script>
            <?php endif; ?>
        </ul>
    </nav>
    <div>
        <a>
            <button class="c-hamburger c-hamburger--htx">
                <span>toggle menu</span>
            </button>
        </a>
    </div>
    <h1 class="logo">
        <a href="Home.php"><img src="images/LOGO-2.png" alt="THE CIRCLE"></a>
    </h1>
    <script>

        //---------------------------cssAnimation----------------------------------------

        var NavTabletLi = $('#NavTablet ul li');
        $.fn.extend({
            animateCss: function (animationName) {
                var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
                this.addClass('animated ' + animationName).one(animationEnd, function () {
                    $(this).removeClass('animated ' + animationName);
                });
            }
        });

        //---------------------------hamburger icon----------------------------------------

        $('.c-hamburger').click(onClick);
        function onClick() {
            var that = $(this);
            var NavTablet = $('#NavTablet');
            var NavTabletbar = $('#NavTablet ul li .botbar');

            if (that.hasClass('is-active')) {
                that.removeClass('is-active');
                NavTablet.fadeOut();
                NavTabletLi.removeClass('slideInDown');
                NavTabletLi.animateCss('slideOutUp');
                NavTabletbar.animate({
                    width: '0%',
                    left: '-110%',
                    opacity: '0'
                },500)
            } else {
                that.addClass('is-active');
                NavTablet.fadeIn();
                NavTablet.css('display', 'inline');
//                $('#NavTablet ul li a').addClass('animated slideInDown');
                NavTabletLi.removeClass('slideOutUp');
                NavTabletbar.animate({
                    width: '60%',
                    opacity: '1',
                    left: '20%'
                },800)

            }
            NavTabletLi.animateCss('slideInDown');
            setTimeout(function () {
                $('.c-hamburger').off('click');
            }, 1);
            NavTabletLi.animateCss('slideInDown');
            setTimeout(function () {
                $('.c-hamburger').on('click', onClick);
            }, 1000)
        }
//---------------------------------購物車數量---------------------------------
        $.get('add_to_cart.php', function(data){
            calItems(data);
        }, 'json');

        function calItems(obj){
            var total = 0;
            for(var s in obj){
                total += obj[s];
            }

            $('.t-badge').text( total);
        }
    </script>
    <nav class="stroke">
        <ul>
            <li><a href="onsale.php">最新優惠</a></li>
            <li><a href="productlist.php">全部商品</a></li>
            <li><a href="lookbook.php">形象特輯</a></li>
            <li><a href="about.php">關於我們</a></li>
            <li><a href="customize.php">客製服務</a></li>
        </ul>
    </nav>
</header>
<!-----------secnav----------------->
<div class="secnav">
    <div class="secnav-right">
        <ul>
            <li class="secnav-right signIn">
                <a href=""><img src="images/user-icon.png" alt=""></i></a>
            </li>
            <li class="secnav-right members">
                <a href=""><img src="images/user-icon-1.png" alt=""></i></a>
            </li>
            <li class="secnav-right wish-list">
                <a href=""><i class="fa fa-heart" aria-hidden="true"></i></a>
            </li>
            <li class="secnav-right bag">
                <a href=""><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
            </li>
        </ul>
    </div>
</div>
<div class="addCart">
    <span class="addCartText">成功加入購物車!!</span>
</div>
<style>
    .addCart{
        text-align:center;
        position:fixed;
        overflow:hidden;
        background-color:white;
        border: solid 1px rgb(204,169,112);
        color:rgb(204,169,112);
        transition:all 0.3s linear;
        width:200px;
        height:100px;
        top:calc(50% - 50px);
        right:-200px;
        z-index:999999;
        overflow:hidden;
        opacity:0;
        /*border-radius:10%;*/
    }
    .addCart .addCartText{
        display:block;
        margin:auto;
        padding-top:20%;
    }
</style>

