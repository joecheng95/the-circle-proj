<?php
require __DIR__ . "/__connect_db.php";
$pname = "product_list";

//$c_rs = $mysqli->query("SELECT * FROM `the circle`");

//echo '$c_rs = ' ; //抓出categories
//var_dump($c_rs);
//echo '<br>';

//while($r = $c_rs->fetch_assoc()){
//    $cates[ $r['sid'] ] = $r; //$r['sid']當作是key, $r是value
//}
//echo '$r = ' ;
//var_dump($r);
//echo '<br>';

//echo '$cates = ' ;
//var_dump($cates);
//echo '<br>';

//$c_rs->data_seek(0);
//$a = $c_rs->data_seek(0);
//echo '$a = ' ;
//var_dump($a);
//echo '<br>';


/*
 *
 * print_r($cates[5]);
 *
$c_rs->data_seek(0);
while($r=$c_rs->fetch_assoc()){
    $cates[] = $r;
}
*/
//$cate = isset($_GET["cate"]) ? (int)$_GET["cate"] : 0;
//$has_cate = !empty( $cates[$cate] ); //有沒有這個分類
////echo $has_cate ? '有' : '無';
//
//$p_where = ' WHERE 1 ';
//if($has_cate) {
//    $p_where .= " AND `category_sid`=$cate ";
//}


//取得總筆
$t_rs = $mysqli->query("SELECT COUNT(1) FROM `the circle`");
$t_rows = $t_rs->fetch_row();
$num_rows = $t_rows[0];

//取得該頁的資料
$page = isset($_GET["page"]) ? (int)$_GET["page"] : 1;
$per_page = 120;
$num_pages = ceil($num_rows / $per_page);

$sql = sprintf("SELECT * FROM `the circle` WHERE sid<107 LIMIT %s, %s", ($page - 1) * $per_page, $per_page);

$rs = $mysqli->query($sql);

//------

//$c2_rs = $mysqli->query("SELECT * FROM `the circle` ");
//while($r2=$c2_rs->fetch_assoc()){
//    $cates2[] = $r2;
//}

//$mmenu = array();
//foreach($cates2 as $c) {
//    if($c['parent_sid']==0){
//        $mmenu[$c['sid']] = $c;
//    }
//}


//foreach($cates2 as $c) {
//    if(!empty( $mmenu[$c['parent_sid']] )){
//        $mmenu[$c['parent_sid']]['sub'][] = $c;
//    }
//}

//print_r($mmenu);
//exit;
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <?php include __DIR__ . '/__page_head.php' ?>
    <link rel="stylesheet" type="text/css" href="css/productlist.css">
    <link rel="stylesheet" href="css/jquery.lazyloadxt.fadein.css">
    <script type="text/javascript" src="js/jquery.lazyloadxt.js"></script>
    <script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
</head>
<body>
<?php include __DIR__ . '/__page_header.php' ?>
<button class="goTop"><span class="label">TOP</span></button>
<div class="container-fluid">
    <div class="col-md-3 col-xs-12">
        <div class="check_menu_group">
            <div class="button-group filter-button-group col-md-12 col-xs-6">

                <div class="check_title">材質</div>
                <ul>
                    <li>
                        <label>
                            <div data-filter="*"> 所有材質</div>
                        </label>
                    </li>
                    <li>
                        <label>
                            <div data-filter=".銀">925純銀</div>
                        </label>
                    </li>
                    <li>
                        <label>
                            <div data-filter=".鋼">西德鋼</div>
                        </label>
                    </li>
                    <li>
                        <label>
                            <div data-filter=".合金, .K金, .白K">其他</div>
                        </label>
                    </li>
                </ul>
            </div>
        </div>
        <div class="check_menu_group">
            <div class="button-group filter-button-price col-md-12 col-xs-6">

                <div class="check_title">價格</div>
                <ul>
                    <li>
                        <label>
                            <div class="filter-button-price1" data-filter="numberLowerThan800"> 800以下</div>
                        </label>
                    </li>
                    <li>
                        <label>
                            <div class="filter-button-price2" data-filter="number800To1500">800~1500</div>
                        </label>
                    </li>
                    <li>
                        <label>
                            <div class="filter-button-price3" data-filter="number1500Plus">1500以上</div>
                        </label>
                    </li>
                </ul>
            </div>
        </div>
        <div class="check_menu_group">
            <div class="button-group filter-button-group col-md-12 col-xs-6">

                <div class="check_title">款式</div>
                <ul id="filters">
                    <li>
                        <label>
                            <div data-filter=".刻字">刻字</div>
                        </label>
                    </li>
                    <li>
                        <label>
                            <div data-filter=".情侶">情侶</div>
                        </label>
                    </li>
                    <li>
                        <label>
                            <div data-filter="*">閨密</div>
                        </label>
                    </li>
                    <li>
                        <label>
                            <div data-filter=".簡約">簡約</div>
                        </label>
                    </li>
                </ul>
            </div>
        </div>


    </div>
    <!--    <nav aria-label="Page navigation">-->
    <!--        <ul class="pagination">-->
    <!--            --><?php //for($i=1; $i<=$num_pages; $i++):
    //
    //                $qr = array(
    //                    'page' => $i,
    //                );
    //                ?>
    <!--                <li class="--><? //= $page==$i ? 'active' : '' ?><!--">-->
    <!--                    <a href="?--><? //= http_build_query($qr) ?><!--">--><? //=$i?><!--</a>-->
    <!--                </li>-->
    <!--            --><?php //endfor ?>
    <!---->
    <!--        </ul>-->
    <!--    </nav>-->

    <div class="grid col-md-9 col-xs-12 product_wrap">

        <?php while ($row = $rs->fetch_assoc()): ?>

            <div class="col-md-3 grid-item col-xs-12 <?= $row['category_sid'] ?> <?= $row['material'] ?> <?= $row['species'] ?>">
                <div class="product_box">
                    <div class="product_pic">
                        <a href="product.php?sid=<?= $row['sid'] ?>">
                            <img class="lazy" data-src="images/allproducts/shop<?= $row['sid'] ?>.jpg" alt="">
                        </a>
                    </div>
                    <h3 class="product_name"><?= $row['name'] ?></h3>
                    <h4 class="price">TWD <?= $row['price'] ?></h4>
                    <!--                <a class="btn btn-warning btn-sm buy_btn" data-sid="-->
                    <? //= $row['sid'] ?><!--" style="background-color:#c9e2e0;background-image:none;border-color:transparent;color:black;width:200px;">加入購物車</a>-->
                    <div class="relacont">
                        <a class="btn btn-sm buy_btn" data-sid="<?= $row['sid'] ?>">
                            <div class="cartsucc">加入成功</div><span>加入購物車</span>
                        </a>

                    </div>

                </div>
            </div>
        <?php endwhile; ?>

    </div>

</div>
<script>
    function topFunction() {
        document.body.scrollTop = 0; // For Chrome, Safari and Opera
        document.documentElement.scrollTop = 0; // For IE and Firefox
    }
    var goTop = $('.goTop');
    var windowHeight = $(window).height();

    $(window).scroll(function(){
        if ($(window).scrollTop()>20){
            goTop.css({'opacity':'1', 'bottom':'7%'})
        }else{
            goTop.css({'opacity':'0', 'bottom':'0'})
        }
    });

    goTop.click(function(){
       $('body').animate({scrollTop:0},1000)
    });

    $('.buy_btn').click(function () {
        var sid = $(this).attr('data-sid');
//        var qty = $(this).closest('.thumbnail').find('.qty').val();
        var qty = 1;
        var productname = $(this).parent().find('h3').text();
        var addCart = $('.addCart');
        var cartsucc = $(this).closest('.relacont').find('.cartsucc');

        $.get('add_to_cart.php', {sid: sid, qty: qty}, function (data) {
//            alert(productname + ' 已加入購物車');
            calItems(data); // 計算並顯示總數量
        }, 'json');

        cartsucc.css({'transform':'translateY(29px)', 'border':'solid 1px #a7a7a7'});
        setTimeout(function(){
            cartsucc.css({'transform':'translateY(0)', 'border':'none'})
        },800)

//        addCart.css({'right':'0','opacity':'0.85'});
//        setTimeout(function(){
////            addCart.fadeOut('slow', function(){
//                addCart.css({'right':'-200px','opacity':'0'})
//
//        },1000)
    });

    //    $("#filters :checkbox").click(function() {
    //
    //        var re = new RegExp($("#filters :checkbox:checked").map(function() {
    //            return this.value;
    //        }).get().join("|") );
    //        $("div").each(function() {
    //            var $this = $(this);
    //            $this[re.source!="" && re.test($this.attr("class")) ? "show" : "hide"]();
    //        });
    //    });
    var $grid = $('.grid').isotope({
        // options
    });
    // filter items on button click
    $('.filter-button-group').on('click', 'div', function () {
        var filterValue = $(this).attr('data-filter');
        $grid.isotope({filter: filterValue});
    });


    var filterFns = {
        // show if number is greater than 50
        numberLowerThan800: function () {
            var number = parseInt($(this).find('h4').text().slice(4, 8));
            return parseInt(number, 10) < 800;
        }
    };
    var filterFns2 = {
        // show if number is greater than 50
        number800To1500: function () {
            var number = parseInt($(this).find('h4').text().slice(4, 8));
            return parseInt(number, 10) < 1500 && parseInt(number, 10) > 800;
        }
    };

    var filterFns3 = {
        // show if number is greater than 50
        number1500Plus: function () {
            var number = parseInt($(this).find('h4').text().slice(4, 8));
            return parseInt(number, 10) > 1500;
        }
    };

    $('.filter-button-price1').on('click', function () {
        var filterValue = $(this).attr('data-filter');

        // use filter function if value matches
        filterValue = filterFns[filterValue] || filterValue;

        $grid.isotope({filter: filterValue});

    });

    $('.filter-button-price2').on('click', function () {
        var filterValue2 = $(this).attr('data-filter');
        filterValue2 = filterFns2[filterValue2] || filterValue2;
        $grid.isotope({filter: filterValue2});

    });

    $('.filter-button-price3').on('click', function () {
        var filterValue3 = $(this).attr('data-filter');
        filterValue3 = filterFns3[filterValue3] || filterValue3;
        $grid.isotope({filter: filterValue3});

    });

    $('.button-group ul li label div').on('click', function (){
//        $(this).closest('li').siblings().css('background-color','transparent');
        $('.button-group ul li').css('background-color','transparent');
        $(this).closest('li').css('background-color','rgba(201, 226, 224, 0.8)')
    });

</script>
<?php include __DIR__ . '/__page_footer.php' ?>

</body>
</html>