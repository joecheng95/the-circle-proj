<?php
require __DIR__ . '/__connect_db.php';
$pname = 'couponback';

$result = array(
    'success' => false,
    'coupon' => [],
    'msg' => '',
);
// check member login
if(isset($_SESSION['user'])) {
    // get ?sel=1  //012

    $sel = intval($_GET['sel']);


    $coupon = array(0, 50, 100);
    shuffle($coupon);
//print_r($coupon);

    $result = array(
        'success' => false,
        'coupon' => $coupon,
        'sel' => $coupon[$sel],
    );


    $today = date("Y-m-d");
    $sql = sprintf("SELECT * FROM `members` WHERE id=%s AND coupondate<>'%s' ",
        $_SESSION['user']['id'],
        $today
    );

    $c_rs = $mysqli->query($sql);

    if ($row = $c_rs->fetch_assoc()) {
        $sql = sprintf("UPDATE `members` SET `couponpt`=%s,`coupondate`='%s' WHERE id=%s",
            $row['couponpt'] + $coupon[$sel],
            $today,
            $_SESSION['user']['id']);

        $c_rs = $mysqli->query($sql);
        $result['success'] = true;
        $_SESSION['user']['couponpt'] = $coupon[$sel];
    } else {
        $result['msg'] = 'already';

    }

}
echo json_encode($result);
