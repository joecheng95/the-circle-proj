<?php
require __DIR__ . '/__connect_db.php';
$pname = 'login';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include __DIR__ . '/__page_head.php' ?>
    <link rel="stylesheet" type="text/css" href="css/Homenew.css">
    <link rel="stylesheet" type="text/css" href="slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>
</head>
<body>
<?php include __DIR__ . '/__page_header.php' ?>
<div class="slickslide">
    <div>
        <img src="images/homeimg/homeslide_a.jpg" alt="">
    </div>
    <div>
        <img src="images/homeimg/homeslide_b.jpg" alt="">
    </div>
    <div>
        <img src="images/homeimg/homeslide_c.jpg" alt="">
    </div>
    <div>
        <img src="images/homeimg/homeslide_d.jpg" alt="">
    </div>
</div>
<section class="section1 container-fluid">
    <div class="item">
        <div class="item1 itemimg">
            <img src="images/homeimg/newarrival.jpg" alt="">
        </div>
        <div class="item2 itemimg">
            <img src="images/homeimg/newarrival.png" alt="">
        </div>
    </div>
</section>
<section class="section2 container-fluid">
    <div class="hotitem">
        <div class="hotitem1 hotimg">
            <img src="images/homeimg/hot-items.jpg" alt="">
        </div>
        <div class="hotitem2 hotimg">
            <div class="hot hot1">
                <img src="images/homeimg/hot1.jpg" alt="">
            </div>
            <div class="hot hot2">
                <img src="images/homeimg/hot2.jpg" alt="">
            </div>
        </div>
        <div class="hotitem3 hotimg">
            <img src="images/homeimg/hot3.jpg" alt="">
        </div>
    </div>
</section>
<section class="section3 container-fluid">
    <div class="eventitem">
        <div class="eventimg eventitem1">
            <img src="images/homeimg/event1.jpg" alt="">
        </div>
        <div class="eventimg eventitem2">
            <img src="images/homeimg/events.png" alt="">
        </div>
        <div class="eventimg eventitem3">
            <img src="images/homeimg/event2.jpg" alt="">
        </div>
    </div>
</section>
<section class="section4 container-fluid">
    <div>
        <div>
            <img src="images/homeimg/lookbook.jpg" alt="">
        </div>
    </div>
</section>
<section class="section5 container-fluid">
    <div class="lookbook">
        <div class="lbimg lbimg1">
            <img src="images/homeimg/look1.jpg" alt="">
        </div>
        <div class="lbimg lbimg2">
            <img src="images/homeimg/look2.jpg" alt="">
        </div>
        <div class="lbimg lbimg3">
            <img src="images/homeimg/look3.jpg" alt="">
        </div>
        <div class="lbimg lbimg4">
            <img src="images/homeimg/look4.jpg" alt="">
        </div>
    </div>
</section>

<script type="text/javascript" src="slick/slick.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var homeSlide = $('.slickslide');
        homeSlide.slick({
            dots: true,
            infinite: true,
            speed: 500,
            draggable: true,
            cssEase: 'linear',
            arrows: false,
            autoplay: true,
            autoplaySpeed: 2000,
            fade: true

        })
    });
    $(window).trigger('scroll');

    $(window).scroll(function(){
        var window_height = $(window).height() ;//-----------網頁看到範圍高度
        var window_scroll_top = $(window).scrollTop();//--捲軸位置

        //----------------------------------------------第一層
        var item1 = $('.item1');
        var item1_top = $('.item1').offset().top ;
        var item1_height = item1.height() *0.5;
        var item1_arrival = item1_top + item1_height - window_height ;//--item1開始動的位置
        var item2 = $('.item2');
        var item2 = $('.item2');

        if (window_scroll_top > item1_arrival ){
            item1.css({
                'transition': 'all 1s ease ',
                'transform': 'translateX(0%)',
                'opacity': '1'

            });
            item2.css({
                    'transition': 'all 1s ease ',
                    'transform': 'translateX(0%)',
                    'opacity': '1'
                });
            }
        //----------------------------------------------第二層
        var section2 = $(".section2");
        var section2_top = section2.offset().top ;
        var section2_height = section2.height() *0.5;
        var section2_arrival = section2_top + section2_height - window_height ;//--開始動的位置
        if (window_scroll_top > section2_arrival ){
            var hotitem1 = $(".hotitem1.hotimg");
            var hot1 = $(".hot.hot1");
            var hot2 = $(".hot.hot2");
            var hotitem3 = $(".hotitem3.hotimg");
            hotitem1.css({
                'transition': 'all 1s ease ',
                'transform': 'translateX(0)',
                'opacity': '1'
            });
            hot1.css({
                'transition': 'all 1s ease ',
                'transform': 'translateY(0)',
                'transition-delay': '.2s',
                'opacity': '1'
            });
            hot2.css({

                'transition': 'all 1s ease ',
                'transform': 'translateY(0)',
                'transition-delay': '.2s',
                'opacity': '1'
            });
            hotitem3.css({
                'transition': 'all 1s ease ',
                'transform': 'translateX(0)',
                'opacity': '1'
            });
        }
        //-----------------------------------------------第三層
        var section3 = $(".section3");
        var section3_top = section3.offset().top ;
        var section3_height = section3.height() *0.5;
        var section3_arrival = section3_top + section3_height - window_height ;//--開始動的位置
        if (window_scroll_top > section3_arrival ){
            var eventitem1 = $(".eventimg.eventitem1");
            var eventitem2 = $(".eventimg.eventitem2");
            var eventitem3 = $(".eventimg.eventitem3");
            eventitem1.css({
                'transition': 'all 1s ease , opacity 1s ease .35s',
                'transform': 'translateY(0%)',

                'opacity': '1'
        });
            eventitem2.css({
                'transition': 'all 1s ease, opacity 1s ease .35s',
                'transform': 'translateY(0%)',
                'opacity': '1'
            });
            eventitem3.css({
                'transition': 'all 1s ease, opacity 1s ease .35s',
                'transform': 'translateY(0%)',
                'opacity': '1'
            });
        }
        //----------------------------------------------lookbook下面那層
        var section5 = $(".section5");
        var section5_top = section5.offset().top ;
        var section5_height = section5.height() *0.5;
        var section5_arrival = section5_top + section5_height - window_height ;//--開始動的位置
        if (window_scroll_top > section5_arrival ){
            var lbimg1 = $(".lbimg.lbimg1 img");
            var lbimg2 = $(".lbimg.lbimg2 img");
            var lbimg3 = $(".lbimg.lbimg3 img");
            var lbimg4 = $(".lbimg.lbimg4 img");
            lbimg1.css({
                'transform': 'translateX(0%) translateY(0%) translateZ(0px) rotateX(0deg) rotateY(0deg)',
                'transition': 'all 1s ease, rotateX 1s ease .35s, rotateY 1s ease .55s',
                'opacity': '1'
            });
            lbimg2.css({
                'transform': 'translateX(0%) translateY(0%) translateZ(0px) rotateX(0deg) rotateY(0deg)',
                'transition': 'all 1s ease .2s, rotateX 1s ease .55s, rotateY 1s ease .75s',
                'opacity': '1'
            });
            lbimg3.css({
                'transform': 'translateX(0%) translateY(0%) translateZ(0px) rotateX(0deg) rotateY(0deg)',
                'transition': 'all 1s ease .4s, rotateX 1s ease .75s, rotateY 1s ease .95s',
                'opacity': '1'
            });
            lbimg4.css({
                'transform': 'translateX(0%) translateY(0%) translateZ(0px) rotateX(0deg) rotateY(0deg)',
                'transition': 'all 1s ease .6s, rotateX 1s ease .95s, rotateY 1s ease 1.05s',
                'opacity': '1'
            });
        }
    })
</script>
<?php include __DIR__ . '/__page_footer.php' ?>
</body>
</html>