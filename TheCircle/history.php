<?php
require __DIR__ . '/__connect_db.php';
$pname = 'history';

if(!isset($_SESSION['user'])){
    exit;
}

$t = time() - 180*24*60*60;
$t2 = date('Y-m-d', $t);
/*
$sql = sprintf("SELECT * FROM `orders` WHERE `member_sid`=%s AND `order_date` > '%s' ORDER BY `order_date` DESC",
        $_SESSION['user']['id'],
        $t2
    );
*/

$sql = sprintf("SELECT o.`amount`, o.`order_date`, o.`couponcheck`,
  d.`order_sid`, d.`product_sid`, d.`price`, d.`quantity`,
  p.`name`
FROM `orders` o
JOIN `order_details` d ON o.sid=d.order_sid
JOIN `the circle` p ON d.product_sid=p.sid
WHERE o.`member_sid`=%s AND o.`order_date` > '%s'
ORDER BY o.`order_date` DESC;",
    $_SESSION['user']['id'],
    $t2
);
//echo $sql;
//exit;
$rs = $mysqli->query($sql);

$data = array();

$order_sid = 0;
$index = -1;
while($row = $rs->fetch_assoc()):
    if($order_sid != $row['order_sid']):
        $order_sid = $row['order_sid'];
        $index++;
        $data[$index] = array(
            'order_sid' => $row['order_sid'],
            'amount' => $row['amount'],
            'order_date' => $row['order_date'],
            'couponcheck' => $row['couponcheck'],
            //'details' => array(),
        );
    endif;
    $data[$index]['details'][] = array(
        'product_sid' => $row['product_sid'],
        'price' => $row['price'],
        'name' => $row['name'],
        'quantity' => $row['quantity'],
    );

endwhile;
?>
<!doctype html>
<html lang="en">
<title>Document</title>
<head>
    <?php include __DIR__ . '/__page_head.php' ?>
</head>
<style>
    .container{
        max-width: 1000px;
    }
    .tableTitle {
        background: #3e3d3d;
        color: white;
        font-size: 18px;
        font-weight: 450;
        text-align: center;
        padding: 1% 0 1% 0;
        margin-bottom: 20px;
        border-bottom: solid 1px #c9e2df;
    }
    .coupon.tableTitle{
        margin-top: 50px;
    }
    .table-hover td{
        font-size: 16px;
        color: dimgrey;
    }
    .glyphicon-remove{
        color: rgb(204,169,112);
    }

</style>
<body>
<?php include __DIR__ . '/__page_header.php' ?>
<div class="container">
    <div class="col-md-12">
        <p class="tableTitle">訂單查詢</p>
<table class="table table-hover">

    <thead>
    <tr>
        <th>#</th>
        <th>訂購日期</th>
        <th>訂購編號</th>
        <th>使用購物金</th>
        <th>付款方式</th>
        <th>應付金額</th>
        <th>訂單取消</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($data as $o_item): ?>
    <tr>
        <td><?= $o_item['order_sid'] ?></td>
        <td><?= $o_item['order_date'] ?></td>
        <td><?= $o_item['order_sid'] ?></td>
        <td><?= $o_item['couponcheck'] ?></td>
        <td>宅配線上付款</td>
        <td>TWD <?= $o_item['amount'] ?></td>
        <td><div class="glyphicon glyphicon-remove"></div></td>
    </tr>
    <?php endforeach; ?>

    </tbody>
</table>
        <p class="coupon tableTitle">購物金查詢</p>
        <p>目前擁有購物金：<span class="coupon_dollar"><?= $_SESSION['user']['couponpt'] ?></span></p>
    </div>
</div>
</body>
</html>