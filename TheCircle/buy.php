<!DOCTYPE html>
<html lang="en">
<head>
    <?php include __DIR__ . '/__page_head.php' ?>
</head>
    <style>
        .circle{

            width: 500px;
            height: 500px;
            margin: 0 auto;
            border-radius: 50%;
            border: 0.2em solid #626262;
            background-color: white;

        }
        .circle .succ{
           display: block;
            margin: auto;
            margin-top: 130px;

        }

        .circle img{
            width: 50%;
        }
        .circle .text{
            text-align: center;
            padding-top:45px ;
        }
        .circle h{
            font-size: 30px;
            font-family:'微軟正黑體';
            color: #626262;
            letter-spacing:2px;
        }
        .circle p{
            font-size: 20px;
            font-family:'微軟正黑體';
            color: #000000;
        }
        
        .circle .shop{
            color: rgb(204,169,112);
        }

        .circle .home{
            width: 144px;
            height:28px;
            border-radius: 5px;
            background-color:#cae2df;
            display: block;
            margin: auto;
            text-align: center;
            color: white;
            font-size: 15px;
            margin-top: 42px;
        }
        /*.home-text{*/
            /*padding: 3.5px;*/
            /*font-family:'微軟正黑體';*/
            /*letter-spacing:3px;*/
        /*}*/

        .circle button{
            width: 144px;
            height: 28px;
            background-color:#cae2df;
            color:white;
            font-size:15px;
            border-radius:5px;
            letter-spacing:3px;
            font-family:'微軟正黑體';
        }


    </style>
<body>


<div class="circle">
    <div>
        <img class="succ" src="images/Success.png" alt="">
        <div class="text">
            <h1>訂單已成立，感謝您的訂購!</h1>
            <p>如預查詢訂單，請至會員中心<a class="shop" href="#">訂購查詢</a></p>
        </div>
        <div class="home">

            <!--<div class="home-text">返回首頁</div>-->

            <button>返回首頁</button>


        </div>


    </div>


</div>

</body>
</html>