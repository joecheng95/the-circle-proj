<?php
require __DIR__ . '/__connect_db.php';
$pname = 'coupon';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>天天抽購物金</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <style>
        .container{
            max-height: 800px;
            width: 900px;
            background: url("images/coupon/background1.png") 0 0 no-repeat;
            display: flex;
            padding:300px 90px 0 90px;
        }
        .card {
            /*box-shadow:25px 25px 50px;*/
            height: 280px;
            width: 200px;
            border-radius: 12px;
            /*overflow:hidden;*/
            margin: 0 auto;
            position: relative;
            transform-style: preserve-3d;
            cursor: pointer;
        }

        .card img{
            border-radius:12px;
            height:280px;
            width:100%;
            display:flex;
            backface-visibility:hidden;


        }
        .back.img-thumbnail{
            box-shadow:5px 5px 5px rgba(220,220,220,.8);
        }

        .cardtop{
            z-index:100;
            transition:all 1s cubic-bezier(0,.98,.42,1.27);
            transform:rotateY(-180deg);
        }
        .transition.flip{
            /*transition:all 1s linear;*/
            /*transition:all 1s cubic-bezier(0,.98,.42,1.27);*/
            transform:rotateY(0deg);
        }

        .transition.flipPick{
            transform:scale(1.2);
        }
        
/*        .transition .front{
            transform:rotateY(180deg);
        }
      */
        
        .front, .back{
            position:absolute;
        }
        .front{
            z-index:15;
        }
        .back{
            transform:rotateY(-180deg);
            z-index:10;
        }
        .front1{
            top:0;
            left:0;
        }
        .draw{
            font-size: 30px;
            color: rgb(204,169,112);
            text-align: center;
            margin-top: 30px;
        }



    </style>
</head>
<body>

    <div class="container  col-md-12">
        <div class="card cardtop transition">
            <img class="back img-thumbnail" src="images/coupon/cardback1.png">
            <img class="front front3 img-thumbnail" src="images/coupon/cardface0.png">
        </div>
        <div class="card cardtop transition">
            <img class="back img-thumbnail" src="images/coupon/cardback2.png">
            <img class="front front2 img-thumbnail" src="images/coupon/cardface50.png">
        </div>
        <div class="card cardtop transition">
            <img class="back img-thumbnail" src="images/coupon/cardback3.png">
            <img class="front front1 img-thumbnail" src="images/coupon/cardface100.png">
        </div>

    </div>
    <div class="col-md-12">
        <div class="draw"></div>
    </div>


    <script
            src="https://code.jquery.com/jquery-2.2.4.min.js"
            integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
            crossorigin="anonymous">
    </script>
    <script>
    $(".transition").on('click',function(){
        var sflip = $(this).siblings();
        var that = $(this);
            $(this).addClass('flip');
        setTimeout(function () {
            that.addClass('flipPick');
        },1000);
        setTimeout(function () {
            sflip.off('click');

        },1);

            setTimeout(function () {
                sflip.addClass('flip');

            },2000);

        // alert($(this).index());
        var sel = $(this).index();

        $.get('couponback.php', {sel:sel} , function(data){
            if(data.success){
                $(".transition").eq(0).find('.front').attr('src', 'images/coupon/cardface'+ data.coupon[0] + '.png');
                $(".transition").eq(1).find('.front').attr('src', 'images/coupon/cardface'+ data.coupon[1] + '.png');
                $(".transition").eq(2).find('.front').attr('src', 'images/coupon/cardface'+ data.coupon[2] + '.png');
                $(".draw").html("恭喜獲得" + "<span>" + data.coupon[sel] + "</span>元購物金")

            } else{
                $(".transition").off('click');
                $(".draw").html("<p>今日已抽過!</p>")
            }
        }, 'json');
    });







    </script>
</body>
</html>