<!DOCTYPE html>
<html lang="en">
<?php include __DIR__. '/__page_head.php' ?>

<style>
    label{
        font-weight: normal;
        font-size: 16px;
        padding-left: 2%;
    }

    .check_all{
        padding: 10%;
    }
    .check_all span{
        padding-left: 10px;
    }
    .container-fluid .row .product_wrap{
        padding-top: 2%;
        text-align: center;
    }

    .product_box{
        width: 200px;
        text-align: center;
        margin-bottom: 15%;
    }
    .product_box .product_pic img{
        width: 200px;
        height: auto;
        border: solid 1px #f3f3f3;
    }
    .product_box .product_pic{
        width: 200px;

        position: relative;
    }
    .product_box .product_pic .product_check{
        position: absolute;
        top: 5%;
        left: 6%;
    }


    .product_box .product_name{
        margin: 5px 0 5px 0;
        font-size: 16px;
        font-weight: 400;
    }
    .product_box .price{
        margin: 5px 0 5px 0;
        color:#8a8a8a;
        font-size: 14px;
        font-weight: 400;
    }
    .container-fluid{
        padding-left:10%;
        padding-right:10%;
    }

    .container-fluid .row .add_too_bag_btn{
        background-color: #c9e2df;
        color: white;
        padding: 5px 4% 5px 4%;
        transform: translateX(30%);
    }


    /*----------------麵包屑-----------------*/
    .container-fluid .row .breadcrumb{
        background-color: transparent;

        font-size: 14px;
    }

    /*!*--------------media Q--------------------*!*/
    @media screen and (max-width: 600px) {
        .container-fluid .product_box{
            width: 100%;
        }
        .container-fluid .product_box .product_pic img {
            width: 100%;
        }
        .container-fluid .product_box .product_pic{
            width: 100%;
        }

        .container-fluid .row{
            min-width: 100%;
        }

        .container-fluid .row .product_wrap{
            padding-top: 10%;
        }
        .container-fluid .row .add_too_bag_btn{
            transform: translateX(10%);
        }


    }

</style>

<body>
<?php include __DIR__. '/__page_header.php' ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
            <ol class="breadcrumb">
                <li><a href="#">首頁</a></li>
                <li><a href="#">會員中心</a></li>
                <li class="active">收藏清單</li>
            </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 col-xs-12">
                <label>
                    <input class="checkbox_size check_all" type="checkbox" id="checkAll"><span> 勾選/取消全部</span>
                </label>
                <button class="btn add_too_bag_btn">加入購物車</button>
            </div>
        </div>
        <div class="row">
        <div class="col-md-12 product_wrap">
                <div class="col-md-3 col-xs-6">
                    <div class="product_box">
                        <div class="product_pic">

                                <input class="checkbox_size product_check" type="checkbox">

                            <img src="images/product_list/b_1.jpg" alt="">
                        </div>
                        <h3 class="product_name">簡約LOVE字母手環</h3>
                        <h4 class="price">TWD.399</h4>
                    </div>
                </div>
            <div class="col-md-3 col-xs-6">
                <div class="product_box">
                    <div class="product_pic">

                        <input class="checkbox_size product_check" type="checkbox">

                        <img src="images/product_list/b_1.jpg" alt="">
                    </div>
                    <h3 class="product_name">簡約LOVE字母手環</h3>
                    <h4 class="price">TWD.399</h4>
                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <div class="product_box">
                    <div class="product_pic">

                        <input class="checkbox_size product_check" type="checkbox">

                        <img src="images/product_list/b_1.jpg" alt="">
                    </div>
                    <h3 class="product_name">簡約LOVE字母手環</h3>
                    <h4 class="price">TWD.399</h4>
                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <div class="product_box">
                    <div class="product_pic">

                        <input class="checkbox_size product_check" type="checkbox">

                        <img src="images/product_list/b_1.jpg" alt="">
                    </div>
                    <h3 class="product_name">簡約LOVE字母手環</h3>
                    <h4 class="price">TWD.399</h4>
                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <div class="product_box">
                    <div class="product_pic">

                        <input class="checkbox_size product_check" type="checkbox">

                        <img src="images/product_list/b_1.jpg" alt="">
                    </div>
                    <h3 class="product_name">簡約LOVE字母手環</h3>
                    <h4 class="price">TWD.399</h4>
                </div>
            </div>

            </div>
        </div>
        </div>

    </div>



<?php include __DIR__. '/__page_footer.php' ?>


<script type='text/javascript'>

    $("#checkAll").click(function(){
        $ifAll=$(this).prop("checked");
        if($ifAll){

            $(".product_pic :checkbox").prop("checked",true);
        }else{

            $(".product_pic :checkbox").prop("checked",false);
        }
    });
</script>
</body>
</html>