
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/jquery-ui-git.css">
    <link rel="stylesheet" href="colorbox/example1/colorbox.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
<!--    <script src="https://use.fontawesome.com/59c18704f5.js"></script>-->

    <script src="js/jquery-3.2.1.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="https://code.jquery.com/ui/jquery-ui-git.js"></script>
    <script src="colorbox/jquery.colorbox.js"></script>
    <script type="text/javascript" src="js/jquery.preload.min.js"></script>
